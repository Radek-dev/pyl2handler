BEGIN;
    CREATE SCHEMA IF NOT EXISTS LTwo;
COMMIT;

BEGIN;
    CREATE TABLE IF NOT EXISTS LTwo.Ask (
        price double precision,
        quantity double precision,
        book_id bigint,
        server_time bigint,
        my_time bigint,
        session_id integer,
        CONSTRAINT pk_ask PRIMARY KEY (price, book_id)
    );

    CREATE OR REPLACE VIEW LTwo.VwAsk AS
        SELECT price,
               quantity/1000000000 as quantity,
               book_id as book_id,
               to_timestamp(server_time) as server_time,
               to_timestamp(my_time) as my_time,
               session_id,
               price * (quantity/1000000000) as total_usd
        FROM LTwo.Ask
    ;

    CREATE TABLE IF NOT EXISTS LTwo.Bid (
        price double precision,
        quantity double precision,
        book_id bigint,
        server_time bigint,
        my_time bigint,
        session_id integer,
        CONSTRAINT bid_pk PRIMARY KEY (price, book_id)
    );

    CREATE OR REPLACE VIEW LTwo.VwBid AS
        SELECT price,
               quantity/1000000000 as quantity,
               book_id,
               to_timestamp(server_time) as server_time,
               to_timestamp(my_time) as my_time,
               session_id,
               price * quantity/1000000000 as total_usd
        FROM LTwo.Bid
    ;

    CREATE OR REPLACE VIEW LTwo.VwBid2 AS
        SELECT price,
               quantity,
               book_id,
               to_timestamp(server_time) as server_time,
               to_timestamp(my_time) as my_time,
               to_timestamp(my_time)::time as myy_time,
               session_id
        FROM LTwo.Bid
    ;


COMMIT;
