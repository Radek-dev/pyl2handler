select *
from
    ltwo.vwask
where
    session_id=3;

select
    min(price) as best_ask, min(my_time)
from
    ltwo.vwask
where
    session_id=2
group by
    book_id;

select ask.book_id, bid.book_id, ask.price,
       ask.quantity, bid.price, bid.quantity,
       ask.my_time as my_time
from ltwo.vwask ask,
     ltwo.vwbid bid
where ask.book_id=801550895
    and ask.book_id = bid.book_id;



select distinct book_id
from ltwo.vwbid
where session_id =3
order by book_id;

delete from ltwo.ask where session_id=3;
delete from ltwo.bid where session_id=3;

select * from ltwo.ask where session_id=3;
select * from ltwo.bid where session_id=3;

select *
 from ltwo.vwask
 where session_id=3
 order by my_time;

select * from
    ltwo.vwask a inner join
    (
    select book_id, min(price) as price, min(my_time) as my_time
     from ltwo.vwask
     where session_id = 3
     group by book_id
     order by my_time
) as qry on a.book_id=qry.book_id and a.price=qry.price;

select *
from ltwo.vwask a
    inner join ltwo.vwbid b
        on a.book_id = b.book_id
where a.session_id = 3;

/*

            FINAL QUERIES

 */

-- data_table ask
select 'ask' as quote_type, book_id, price, quantity, my_time
from ltwo.vwask
where book_id=801550895
order by price
limit 5;

-- data_table bid
select 'bid' as quote_type, book_id, price, quantity, my_time
from ltwo.vwbid
where book_id=801550895
order by price desc
limit 5;


 -- sessions available, ignore session 1
 select distinct
    book_id, min(my_time), max(price)
from
    ltwo.vwbid
where
    session_id=3
group by
    book_id
order by
    book_id;


 -- start and end times - 2020-04-14 17:33:34, 2020-04-15 19:32:06
select
       min(my_time)::date as start_date,
       min(my_time)::time as start_time,
       max(my_time)::date as end_date,
       max(my_time)::time as end_time
from
     ltwo.vwask
where
    session_id = 1;

-- time period - 0 years 0 mons 0 days -1 hours -4 mins -31.00 secs
select
       min(my_time),
       max(my_time),
       min(my_time) - max(my_time)
from
     ltwo.vwask
where
     session_id=2;

select
       min(my_time),
       max(my_time),
       min(my_time) - max(my_time)
from
     ltwo.vwbid
where
     session_id=2;

-- number of seconds - 93512
select extract(epoch from (select max(my_time) - min(my_time) from ltwo.vwask));

-- number of hours - 25.975
select extract(epoch from (select max(my_time) - min(my_time) from ltwo.vwask))/3600;

-- prices and quotes per book id - it is 50 per collection process
select count(price)
from ltwo.ask
GROUP BY book_id;

select count(price)
from ltwo.ask
GROUP BY book_id;


-- number of book ids - 89099
select count( DISTINCT book_id) from ltwo.ask where session_id=3;
select count( DISTINCT book_id) from ltwo.bid;

-- unique book ids;
select DISTINCT book_id from ltwo.ask;
select DISTINCT book_id from ltwo.bid;

-- min and max book id - 710902885, 712131838
select min(book_id), max(book_id) from ltwo.ask;

-- ordered book ids
select DISTINCT book_id, my_time from ltwo.vwask
order by book_id;


-- order book at one specific point
select
       price,
       quantity,
       book_id,
       my_time,
       'a' as price_type
from
       ltwo.vwask a
where
       a.book_id = 710902885
union
select
       price,
       quantity,
       book_id,
       my_time,
       'b' as price_type
from
       ltwo.vwbid b
where
       b.book_id = 710902885
order by
       price_type, price;

select price, quantity, total_usd, book_id, my_time
from ltwo.vwbid b
where b.book_id = 712127769
order by b.price Desc;

select price, quantity, total_usd, book_id, my_time
from ltwo.vwask a
where a.book_id = 712127769
order by a.price Desc;

-- order book with no point
select
       price,
       quantity,
       book_id,
       my_time,
       'a' as price_type
from
       ltwo.vwask a
union
select
       price,
       quantity,
       book_id,
       my_time,
       'b' as price_type
from
       ltwo.vwbid b
order by
       price_type, price;