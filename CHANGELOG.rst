=========
Changelog
=========

Development version
===================

Version 0.1.3
-------------
- create the db schema


Current version
===============

Version 0.1.2
-------------
- write socket code for `CEX.IO` or `Binance`

Older versions
==============

Version 0.1.1
-------------
- set up Postgres on AWS
- write connection for postgres
- improved ``README.md``
- added ``IO`` module and the ``database.ini`` file to store login details.

Version 0.1.0
-------------

- Gitlab set up, CI working
- Tox working locally
- flake8 working in CI and locally
- Versioning using git tags
- Sphinx working with nice theme
- Sphinx visible at https://radek-dev.gitlab.io/pyl2handler/.
