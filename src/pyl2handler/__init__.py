# -*- coding: utf-8 -*-
"""
Level 2 Data module for Python
==============================
This package focus on handling Level 2 Data.

See https://radek-dev.gitlab.io/pyl2handler/ for the documentation.
"""
from pkg_resources import DistributionNotFound, get_distribution

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = 'unknown'
finally:
    del get_distribution, DistributionNotFound
