"""This modules holds helper functions for the project.
"""

import os
from configparser import ConfigParser


def get_path(subpath: tuple = tuple(), no_src: bool = False) -> str:
    """Returns a tuple of project file paths or specific file paths after the
    ``..src/pyl2handler`` folder if you pass in ``true`` parameter

    Args:
        subpath (tuple): pass in a tuple that represents the correct file path.
        no_src (bool): pass in ``true`` if you don't with to have ``no``
                       src folder.

    Returns:
        str: str of file paths.
    """
    directory = os.path.dirname(os.path.abspath(__file__))
    if no_src:
        directory = directory[:directory.rfind('src')]
    if subpath:
        directory = os.path.join(directory, *subpath)
    return directory


def get_config(filename: tuple = ('io', 'credentials.ini'),
               section: str = 'postgresql_aws') -> dict:
    """The function returns the config parameters based on the
    ``credentials.ini`` file. Pass in the file paths to the file to the
    ``filename`` parameter. Pass in specific ``section`` you would like
    to collect.

    Args:
        filename (tuple): full file path to the ``credentials.ini`` file.
        section (str): section from the ``credentials.ini`` file.

    Returns:
        dict: The dictionary with the parameters
    """
    filename = get_path(filename)
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
    # get section, default to postgresql
    if parser.has_section(section):
        dt = {key: value for (key, value) in parser.items(section)}
    else:
        raise Exception(
            'Section {0} not found in the {1} file'.format(section, filename))
    return dt


def store_queries(key: str) -> tuple:
    """
    This is some basic query store. This has fixed queries.
    ToDo: move this to the database sql script
    Args:
        key (str): pass in a string id for the return values

    Returns:
        tuple: queries that we need

    """
    dct = {'basic':
               ("select *"
                " from ltwo.vwask"
                " where session_id=3;",
                "select *"
                " from ltwo.vwbid"
                " where session_id=3;"),
           'basic4':
               ("select *"
                " from ltwo.vwask"
                " where session_id=4;",
                "select *"
                " from ltwo.vwbid"
                " where session_id=4;"),
           'basic_sorted':
               ("select *"
                " from ltwo.vwask"
                " where session_id=3"
                " order by my_time;",
                "select *"
                " from ltwo.vwbid"
                " where session_id=3"
                " order by my_time;"),
           'basic_sorted4':
               ("select *"
                " from ltwo.vwask"
                " where session_id=4"
                " order by my_time;",
                "select *"
                " from ltwo.vwbid"
                " where session_id=4"
                " order by my_time;"),
           'basic_sorted_3_points':
               ("select *"
                " from ltwo.vwask"
                " where session_id=3"
                " order by my_time;",
                "select *"
                " from ltwo.vwbid"
                " where session_id=3"
                " order by my_time;"),
           'one_point':
               ("select price, quantity"
                " from ltwo.vwask"
                " where book_id = 801550895;",
                "select price, quantity"
                " from ltwo.vwbid"
                " where book_id = 801550895;"),
           'quantities_over_time':
               ("select price, quantity, my_time, book_id"
                " from ltwo.vwask"
                " where session_id=3;",
                "select price, quantity, my_time, book_id"
                " from ltwo.vwbid"
                " where session_id=3;"),
           'data_table_ask':
               ("select 'ask' as quote_type, book_id, price, quantity, my_time"
                " from ltwo.vwask"
                " where book_id=801550895"
                " order by price"
                " limit 5;"),
           'data_table_bid':
               ("select 'bid' as quote_type, book_id, price, quantity, my_time"
                " from ltwo.vwbid"
                " where book_id=801550895"
                " order by price desc"
                " limit 5;")
           }    # noqa: E127
    return dct[key]
