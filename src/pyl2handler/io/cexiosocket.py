"""
Comminting
This module deals with cexio websocket.
Base on `Cex.io project <https://github.com/cexioltd/CEX.IO-Client-Python3.5>`_
"""

import asyncio as aio
import datetime
import hashlib
import hmac
import inspect
import json
import logging as lg
import logging.config
import pandas as pd
import psycopg2 as pg
import random

import websockets
import websockets.http
from pyl2handler.helpers import get_path
from pyl2handler.io.pg import async_insert_df


logging.config.fileConfig(get_path(('logging_config.ini',), True))
logger = lg.getLogger('simpleExample')


# A WebSocketClient state:
CLOSED, CONNECTING, OPEN, READY = range(4)

# The :mod:`cexio.protocols_config` module defines configuration parameters
# for protocol connectivity used by :mod:`cexio.ws_client` and
# :mod:`cexio.rest_client`
protocols_config = {
    'ws': {
        'ping_after': 15,
        'protocol_timeout': 3,
        'timeout': 5,  # real 3-8, more than 18 for testing
        'ensure_alive_timeout': 15 + 3,
        'reconnect': True,
        'resend_subscriptions': True,
        'resend_requests': True,
    },
    'rest': {},
}


def __message_equal_or_less(message, t_message, recno, reverse):
    ret = True

    if recno == 12:
        raise Exception("Recursion limit {} exceeded".format(recno))

    if isinstance(message, dict) and isinstance(t_message, dict):
        # Iterate recursively
        for k, vsm in message.items():
            if k in t_message.keys():
                ret &= __message_equal_or_less(vsm, t_message[k],
                                               recno + 1, reverse)
            else:
                ret = False
        return ret

    # Match leafs, interpreting
    # 'None' on the Right (for left-to-right match call with reverse=False)
    #  as ignoring Left value
    # 'None' on the Left  (for right-to-left match call with reverse=True)
    #  as ignoring Right value
    elif t_message is None and not reverse:
        return True
    elif message is None and reverse:
        return True
    elif isinstance(message, str) and \
            isinstance(t_message, str) and message == t_message:  # equal
        return True
    else:
        return False


def message_equal_or_less(message, t_message):
    """
    True if 'message' equal or less __next_callable 't_message'
    # 'None' value in t_message forces to ignore the value in message

    Args:
        message:
        t_message:

    Returns:

    """
    return __message_equal_or_less(message, t_message, 0, False)


def message_equal_or_greater(message, t_message):
    # True if 'message' equal or greater __next_callable 't_message'
    # 'None' value in t_message forces to ignore the value in message
    return __message_equal_or_less(t_message, message, 0, True)


def message_equal(message, t_message):
    """
    True if 'message' equal to 't_message'
    'None' value in t_message forces to ignore the value in message

    Args:
        message:
        t_message:

    Returns:

    """
    return message_equal_or_less(message, t_message) & \
        message_equal_or_greater(message, t_message)


async def default_message_router_sink(message):
    logger.warning("Router> Unhandled message came \
            to default Router.sink: {}".format(message))
    return message


class ConfigError(Exception):
    """Any configuration error should be covered in this exception
    """
    pass


class ProtocolError(Exception):
    """
    An unexpected message should be covered by this exception to signal that
    protocol changed for example
    """
    pass


class AuthError(Exception):
    """
    Exception for User Authentication errors
    """
    pass


class InvalidMessage(Exception):
    """
    Is used in cases if 'dict' message , presenting JSON, is not of expected
    format. For example, functions like message validator, oid_setter/getter,
    data getters should raise InvalidMessage if KeyError thrown while trying to
    get/set some data from/into message.
    """
    pass


class ErrorMessage(Exception):
    """
    Is used to return error message if it is set in message itself
    CEXIO normally sets the app-level error in the message like following
    {'ok': 'error', 'data': {'error': 'error message', }, }
    """
    pass


class CEXWebSocketAuth:
    # JSON template dict
    _json = {
        'e': 'auth',
        'auth': {
            'key': None,
            'signature': None,
            'timestamp': None,
        },
        'oid': 'auth',
    }

    def __init__(self, config):
        try:
            self._key = config['auth']['key']
            self._secret = config['auth']['secret']

        except KeyError as exc:
            raise ConfigError('Missing key in _config file', exc)

    @staticmethod
    def get_curr_timestamp():
        return int(datetime.datetime.now().timestamp())

    def get_timed_signature(self):
        """
        Returns UNIX timestamp in seconds, and 'timed' signature, which is
        the digest of byte string, compound of timestamp and public key
        """
        timestamp = self.get_curr_timestamp()
        message = "{}{}".format(timestamp, self._key)
        return timestamp, hmac.new(self._secret.encode(), message.encode(),
                                   hashlib.sha256).hexdigest()

    def get_request(self):
        """
        Returns JSON from self._json dict
        The request is valid within ~20 seconds
        """
        timestamp, signature = self.get_timed_signature()
        self._json['auth']['key'] = self._key
        self._json['auth']['signature'] = signature
        # noinspection PyTypeChecker
        self._json['auth']['timestamp'] = timestamp
        return self._json


def create_dict_getter(path):
    if path is not None:
        path = path.split('/')

        def getter(d):
            try:
                for item in path:
                    d = d[item]
                return d
            except KeyError as exc:
                raise InvalidMessage(
                    "Invalid dict getter, can't get from: {}".format(d), exc)
    else:
        def getter(d):
            return d
    return getter


def create_dict_setter(path):
    assert path is not None and path != ''
    path = path.split('/')

    def setter(d, value):
        try:
            for item in path[:-1]:
                d = d[item]
            d[path[-1]] = value
            return d

        except KeyError as exc:
            raise InvalidMessage(
                "Invalid dict setter, can't get from: {}".format(d), exc)

    return setter


class CallChain(object):
    """
    Wraps single callable to support message handling async chain call
    callable may be either coroutine or function
    """

    # TODO: create chain() generator function which is creating
    #  chain call instead of chaining at runtime

    def __init__(self, handler=None):
        """
        method passed processes the message __next_callable
        subscribers are called with its' return value if it is not None
        """
        assert not isinstance(handler, CallChain), "Nesting of \
         CallChain object is not suggested"
        self._next = None
        self._handler = handler

    def get_next_callable(self):
        return self._next

    def bind(self, next_callable):
        # attaches new CallChainNode(__next_callable) to the end of the chain
        assert callable(next_callable)
        node = self
        while node._next is not None:
            node = node._next
        if isinstance(next_callable, CallChain):
            node._next = next_callable
        else:
            node._next = CallChain(next_callable)
        return self

    def unbind(self):
        self._next = None

    def __str__(self, *args, **kwargs):
        return "{} ({}, next: {})".format(self.__class__.__name__,
                                          self._handler, self._next)

    def __add__(self, *args, **kwargs):
        next_callable = args[0]
        return self.bind(next_callable)

    @staticmethod
    def is_user_defined_class(obj):
        cls = obj.__class__
        if hasattr(cls, '__class__'):
            return '__dict__' in dir(cls) or hasattr(cls, '__slots__')
        return False

    @staticmethod
    def is_awaitable(obj):
        """
        There is no single method which can answer in any case, should wait or
        not - so need to create one for the suspected cases : func, coro,
        gen-coro, future, class with sync __call__, class with async __call__,
        sync method, async method

        Args:
            obj:

        Returns:

        """

        if inspect.isawaitable(obj) or inspect.iscoroutinefunction(
                obj) or inspect.iscoroutine(obj):
            return True
        elif inspect.isgeneratorfunction(obj):
            return True
        elif CallChain.is_user_defined_class(obj):
            if hasattr(obj, '__call__'):
                return CallChain.is_awaitable(obj.__call__)
            return False
        else:
            return False

    async def __call__(self, message):
        """
        generic call, which eventually may be regenerated to descope if-logic
        from runtime to construct time

        Args:
            message:

        Returns:

        """

        if self._handler is not None:
            if CallChain.is_awaitable(self._handler):
                message = await self._handler(message)
            else:
                message = self._handler(message)
        if message is not None and self._next is not None:
            logger.debug(
                "    CallChain> chain {} to {}".format(message, self._next))
            message = await self._next.__call__(message)
        return message


class RequestResponseFutureResolver(dict, CallChain):
    """
    Extends CallChain, because needs to manage future here, after successor
    called
    """

    # noinspection PyUnusedLocal
    def __init__(self, *,
                 name=None,
                 op_name_get_path=None,
                 key_set_path=None,
                 key_get_path=None,
                 **kwargs):
        super(CallChain, self).__init__()
        super(dict, self).__init__()

        # generate number, unique within ~24 hours
        # which is used as a key for request/rely pair messages
        self._seqId_base = str(int(datetime.datetime.now().timestamp() * 1000))
        self._seqId_curr_id = 0
        self._name = name
        self._key_setter = create_dict_setter(key_set_path)
        self._key_getter = create_dict_getter(key_get_path)
        # TODO should it be feature of resolver? think no
        self._op_name_getter = lambda x: ''
        if op_name_get_path is not None:
            self._op_name_getter = create_dict_getter(op_name_get_path)

    def get_next_seq_id(self):
        self._seqId_curr_id += 1
        return self.get_seq_id()

    def get_seq_id(self):
        return self._seqId_base + '_' + str(
            self._seqId_curr_id) + '_' + self._name

    def mark(self, request, future):
        try:
            op_name = self._op_name_getter(request)
        except KeyError as excep:
            raise InvalidMessage(
                "Can't get 'op_name' from Request: {}".format(request), excep)

        request_id = self.get_next_seq_id() + op_name
        self[request_id] = request, future
        try:
            self._key_setter(request, request_id)
        except KeyError as excep:
            raise InvalidMessage(
                "Can't set 'key' to Request: {}".format(request), excep)
        return request

    def clear(self):
        for r, f in self.values():
            f.cancel()
        super().clear()

    async def __call__(self, message):
        """
        may raise Exception from successors if resolved,
        return result if resolved, None if not, including the case of error
        while getting id

        Args:
            message:

        Returns:

        """

        try:
            request_id = self._key_getter(message)
        except KeyError as exc:
            logger.warning("Can't get 'key' from Response: \
                {}".format(message), exc)
            # no key for resolving - supposed to be processed
            # further by caller (MessageRouter)
            return None

        if request_id in self.keys():
            # 'resolved' with result or error, raised  by chan calls
            logger.debug("    Resolver> resolve {}".format(message))
            request, future = self.pop(request_id)
            if self.get_next_callable() is not None:
                logger.debug("    Resolver> chain {} \
                    to {}".format(message, self.get_next_callable()))
                try:
                    # In Case of known exceptions,
                    # need to return not None, since response is resolved
                    # no raising, be cause exception should be handled ats
                    # future side but not on callee side
                    message = await self.get_next_callable().__call__(message)
                except ErrorMessage as exc:
                    future.set_exception(exc)
                    return exc
                except InvalidMessage as exc:
                    future.set_exception(exc)
                    return exc
            future.set_result(message)
            return message


class MessageRouter(list):
    """
    Matches incoming messages to pattern messages and routes to coroutines:
    - matching goes through the list of pairs [pattern, coro] until matched and
        coro returns not None;
    - if message matched but rejected by coro returning None, matching moves to
        the next pattern;
    - if message not matched, it is sent to the sink or default sink coroutine;
    - coroutines in the list can be either - single async callback, CallChain,
    other Router
    """

    # noinspection PyUnusedLocal
    def __init__(self, t_messages_entries, *,
                 sink=default_message_router_sink,
                 strict_match=False, **kwargs):
        super().__init__(t_messages_entries)
        self.__sink = sink
        if strict_match:
            self.__matcher = message_equal
        else:
            self.__matcher = message_equal_or_greater

    def __str__(self, *args, **kwargs):
        return "{} ({}) Sink: {}".format(self.__class__.__name__,
                                         super().__str__(), self.__sink)

    def __contains__(self, *args, **kwargs):
        return args[0] in map(lambda t: t[0], self)

    def bind(self, other):
        assert callable(other)
        self.__sink = other
        return self

    def __add__(self, *args, **kwargs):
        self.bind(args[0])
        return self

    async def __call__(self, message):
        for t_message, handler in self:
            if self.__matcher(message, t_message):
                logger.debug(
                    "    Router> route {} to {}".format(message, handler))
                result = await handler(message)
                # processed by handler, ignored(passed back) otherwise
                if result is not None:
                    return result
        logger.debug("    Router> pass {} to {}".format(message, self.__sink))
        return await self.__sink(message)  # send any unprocessed to sink


class CommonWebSocketClient:
    """
    """
    # Append CEXIO client API version to 'user_agent' http header value
    websockets.http.USER_AGENT = ' '.join(
        (websockets.http.USER_AGENT, "cexio/{}".format('1.0b')))

    def __init__(self, cfg):
        try:
            _config = cfg.copy()
            if 'auth' in _config.keys():
                _config['auth'] = "{***}"
            logger.info(
                "WS> User Agent: {}".format(websockets.http.USER_AGENT))
            logger.debug(
                "WS> CEXWebSocketClientProtocol with _config: {}".format(
                    _config))

            self.ws = None
            self.state = CLOSED

            self._send_subscriptions = list()
            self._request_subscriptions = list()

            self._uri = cfg['ws']['uri']
            self._need_auth = cfg['authorize']

            self._timeout = protocols_config['ws']['timeout']
            self._protocol_timeout = protocols_config['ws']['protocol_timeout']
            self._ensure_alive_timeout = protocols_config['ws'][
                'ensure_alive_timeout']
            self._reconnect = protocols_config['ws']['reconnect']
            self._reconnect_interval = lambda: 0.1 + random.randrange(
                300) / 100
            self._resend_subscriptions = protocols_config['ws'][
                'resend_subscriptions']
            self._resend_requests = protocols_config['ws']['resend_requests']

            if self._need_auth:
                self._auth = CEXWebSocketAuth(cfg)

            # Message map for special messages, received while running
            special_message_map = (
                ({'e': 'connected', }, self._on_connected),
                ({'ok': 'error', 'data': {'error': 'Please Login'}, },
                 self._on_not_authenticated),
                ({'e': 'ping', }, self._on_ping),
                ({'e': 'disconnecting', }, self._on_disconnecting),
            )
            self._router = self._base_router = MessageRouter(
                special_message_map)
            self._resolver = None

            self._connecting_lock = aio.Lock()
            self._listener_task = None
            self._routing_on = None
            self._send_error = aio.Future()

        except KeyError as exc:
            raise ConfigError('Missing key in _config file', exc)

    def set_router(self, router):
        self._router = self._base_router.bind(router)

    def set_resolver(self, resolver):
        self._resolver = resolver

    # User methods
    # ------------

    async def connect(self):
        try:
            if self.state != CLOSED:
                return

            self.ws = await aio.wait_for(websockets.connect(self._uri),
                                         self._timeout)
            self.ws.timeout = self._protocol_timeout

            if not self._send_error.done():
                self._send_error.cancel()
            self._send_error = aio.Future()

            message = await self.recv()
            if message_equal_or_greater(message, {'e': 'connected', }):
                logger.info('WS> Client Connected')
            else:
                raise ProtocolError(
                    "WS> Client Connection failed: {}".format(message))

            if self._need_auth:
                await self._authorize()

            self.state = OPEN

        except Exception as exce:
            logger.info(
                "{} (\'{}\') while connecting".format(exce.__class__.__name__,
                                                      exce))
            try:
                if self.ws is not None:
                    await aio.wait_for(
                        self.ws.close_connection(force=True), self._timeout)
            except Exception as ex1:
                logger.error("Exception at close_connection: {}".format(ex1))
            raise exce

    async def run(self):
        assert self._router is not None
        assert self._resolver is not None

        await self.connect()
        if self._routing_on is None:
            self._routing_on = aio.ensure_future(self._routing())

        logger.debug('WS.Client> Routing started')

    async def stop(self):
        if self._routing_on is not None and not self._routing_on.done():
            self._routing_on.cancel()

        if self._listener_task is not None and not self._listener_task.done():
            self._listener_task.cancel()

        logger.debug('WS.Client> Routing stopped')
        self.state = CLOSED
        await aio.wait_for(self.ws.close(), self._timeout)

    async def send(self, message):
        await self._connecting_lock.acquire()
        # noinspection PyUnusedLocal
        try:
            await self._send(message)
        except Exception as exc:
            logger.error(exc)
            raise
        finally:
            self._connecting_lock.release()

    async def recv(self):
        # call recv() without timeout, used only in _routing()
        return await aio.wait_for(self._recv(), self._timeout)

    async def request(self, message):
        # Returns resolved and processed response data in future
        future = aio.Future()
        try:
            request = self._resolver.mark(message, future)
            await self.send(request)
        except Exception as exc:
            future.set_exception(exc)
        return await aio.wait_for(future, self._timeout)

    async def send_subscribe(self, message):
        """saving subscription requests to subscribe after reconnect
        """
        self._send_subscriptions.append(message)
        await self.send(message)

    async def request_subscribe(self, message):
        # saving subscription requests to subscribe after reconnect
        self._request_subscriptions.append(message)
        return await self.request(message)

    # Internals
    # ---------

    async def _send(self, message):
        if isinstance(message, dict):
            message = json.dumps(message)

        logger.debug("WS.Client> {}".format(message))

        try:
            await aio.wait_for(self.ws.send(message), self._timeout)
        except Exception as exc:
            # signal to _routing() coro, which will reconnect() od stop()
            self._send_error.set_exception(exc)
            raise ConnectivityError(exc)  # signal error to client call

    async def _recv(self):
        """
        call ws.recv() without timeout, used only in _routing() and in tests
        not supposed to be called while running, it will simply grab the
        message from the queue - not exactly the one expected

        Returns:

        """

        message = await self.ws.recv()
        try:
            message = json.loads(message)
        except Exception as exc:
            raise ProtocolError(exc)

        logger.debug("WS.Server> {}".format(message))
        return message

    async def _authorize(self):
        await self._send(self._auth.get_request())
        response = await self.recv()
        if message_equal(response,
                         {'e': 'auth', 'ok': 'ok', 'data': {'ok': 'ok'}, }):
            logger.info('WS> User Authorized')

        elif message_equal(response, {'e': 'auth', 'ok': 'error',
                                      'data': {'error': None}, }):
            raise AuthError(
                "WebSocketConnection Authentication failed: {}".format(
                    response['data']['error']))
        else:
            raise ProtocolError(
                "WebSocketConnection Authentication failed: {}".format(
                    response))

    async def _routing(self):
        while True:
            self._listener_task = aio.ensure_future(self._recv())

            done, pending = await aio.wait(
                [self._routing_on, self._listener_task, self._send_error],
                return_when=aio.FIRST_COMPLETED,
                timeout=self._ensure_alive_timeout)

            if self._routing_on in done:
                self._routing_on = None
                return

            elif self._send_error in done:
                logger.info("WS> Client disconnected while sending: {}".format(
                    self._send_error.exception()))

                if not await self._on_disconnected():
                    break

            elif self._listener_task in done:
                # noinspection PyUnusedLocal
                try:
                    message = self._listener_task.result()
                    await self._router(message)

                except ProtocolError:
                    raise
                except aio.CancelledError as exc:
                    logger.error(exc)
                    pass
                except Exception as exc:
                    logger.info("WS> Client disconnected while \
                     receiving: {}".format(exc))

                    if not await self._on_disconnected():
                        break

            elif self._listener_task in pending:
                logger.info("WS> Client timeout")

                self._listener_task.cancel()
                if not await self._on_disconnected():
                    break

    async def _on_disconnected(self):
        try:
            self.state = CLOSED
            self._send_error.cancel()
            await aio.wait_for(self.ws.close(), self._timeout)

        except Exception as exc:
            logger.debug(exc)

        if self._reconnect:
            logger.info("WS> Reconnecting...")
            while True:
                try:
                    await aio.sleep(self._reconnect_interval())
                    await self._connecting_lock.acquire()
                    await self.connect()
                    break
                except Exception as exc:
                    logger.info(exc)
                finally:
                    self._connecting_lock.release()

            aio.ensure_future(self._after_connected())
            ret = True  # continue routing

        else:
            logger.info("WS> Client stopped")
            ret = False  # stop routing

        return ret

    async def _after_connected(self):
        try:
            if self._resend_subscriptions:
                for message in self._send_subscriptions:
                    await self.send(message)
                for message in self._request_subscriptions:
                    await self.request(message)

        except Exception as exc:
            logger.info(exc)

    # Special Message Callbacks
    # -------------------------

    async def _on_connected(self, message):
        logger.info('WS> WebSocket connection established')
        if self._need_auth:
            await self.send(self._auth.get_request())
        else:
            self.state = OPEN
        return message

    async def _on_not_authenticated(self, message):
        self.authorized = True
        logger.warning("WS> User Not Authenticated: {}".format(message))
        # TODO close , reconnect if was authorized
        return message

    async def _on_ping(self, message):
        await self.send({'e': 'pong'})
        return message

    async def _on_disconnecting(self, message):
        logger.info('WS> Disconnecting by Server')
        await self._on_disconnected()
        return message


class InvalidResponseError(Exception):
    """
    Indicates issue with REST resource requested by rest_client
    Is raised on eny not 'OK: 200' response
    """
    pass


class ConnectivityError(Exception):
    """
    Is raised on client caller side, if call to websocket client not executed
    due to connectivity issue
    """
    pass


class WebSocketClientSingleCallback(CommonWebSocketClient):

    def __init__(self, _config):
        super().__init__(_config)

        def validator(message):
            try:
                result = message['ok']
                if result == 'ok':
                    return message['data']
                elif result == 'error':
                    raise ErrorMessage(message['data']['error'])
                else:
                    error = InvalidMessage(message)
            except KeyError:
                error = InvalidMessage(message)
            raise error

        resolver = RequestResponseFutureResolver(name='', op_name_get_path='e',
                                                 key_set_path='oid',
                                                 key_get_path='oid')
        self.message_map = (
            ({'e': None,
              'data': None,
              'oid': None,
              'ok': None, }, resolver + validator),

            ({}, self.on_notification),
        )
        router = MessageRouter(self.message_map, sink=self.on_unhandled)
        self.set_router(router)
        self.set_resolver(resolver)
        self.__notification_future = None

    @staticmethod
    async def on_notification(message):
        # Supposed be redefined for the class or for the given object by user
        logger.debug("WS> Notification: {}".format(message))
        return message

    @staticmethod
    def format_message(e_name, data):
        message = {'e': e_name, }
        if data is not None:
            message['data'] = data
        return message

    @staticmethod
    async def on_error(message):
        logger.error("WS> Resolver not expected response: {}".format(message))
        return message

    @staticmethod
    async def on_unexpected_response(message):
        logger.warning(
            "WS> Resolver not expected response: {}".format(message))
        return message  # pass back to the router to continue matching

    @staticmethod
    async def on_unhandled(message):
        logger.warning("WS> Unhandled message: {}".format(message))

##


class WebSocketClientPublicData(WebSocketClientSingleCallback):
    def __init__(self, _config):
        super().__init__(_config)

        def get_first(t):
            if len(t) == 0:
                return None
            else:
                return t[0]

        async def on_tick(message):
            print("Tick > pair: {}:{}, price: {}".format(
                message['data']['symbol1'],
                message['data']['symbol2'],
                message['data']['price']))
            return message

        async def on_ohlcv_init_new(message):
            entries = message['data']
            print("OHLCV_init_new > pair: {}, {} entries like: {}".format(
                message['pair'],
                len(entries), get_first(entries)))
            return message

        async def on_ohlcv_new(message):
            entries = message['data']
            print("OHLCV_new > pair: {}, {} entries like: {}".format(
                message['pair'],
                len(entries), get_first(entries)))
            return message

        async def on_ohlcv1m(message):
            print("OHLCV_1m > {}".format(message['data']))
            return message

        async def on_md(message):
            # data = message['data']
            # print("MD > pair: {},"
            #       " 'buy_total': {}, 'sell_total': {},"
            #       " 'buy': [{} pairs like: {}],"
            #       " 'sell': [{} pairs like: {}]".
            #       format(data['pair'],
            #              data['buy_total'], data['sell_total'],
            #              len(data['buy']), get_first(data['buy']),
            #              len(data['sell']), get_first(data['sell'])))
            print('message')
            print(message)
            aio.ensure_future(cexiosocket_to_pg(message['data']))
            return message

        async def on_md_grouped(message):
            data = message['data']
            print("MD_groped > pair: {},"
                  " 'buy': [{} items like: {}],"
                  " 'sell': [{} items like: {}]".
                  format(data['pair'],
                         len(data['buy'].items()),
                         get_first(list(data['buy'].items())),
                         len(data['sell'].items()),
                         get_first(list(data['sell'].items()))))
            return message

        async def on_history(message):
            data = message['data']
            print("History > [{} items like: '{}'] \
                  ".format(len(data), get_first(data)))
            return message

        async def on_history_update(message):
            data = message['data']
            if len(data) == 0:
                pass  # skip empty history updates
            else:
                print("History Update> [{} items like: [{}]] \
                      ".format(len(data), get_first(data)))
            return message

        async def on_ohlcv(message):
            print("OHLCV > {}".format(message))
            return message

        async def on_ohlcv24(message):
            print("OHLCV_24 > {}".format(message))
            return message

        async def sink(message):
            event = message['e']
            print("   Uncovered > {}".format(event))
            return event

        self.n_router = MessageRouter((

            # Send: {'e': 'subscribe', 'rooms': ['tickers', ], }
            # Server is notifying on transaction executed on any pair
            # with price
            ({'e': 'tick', }, on_tick),

            # Send: { "e": "init-ohlcv-new", "i": "1m", "rooms":
            # ["pair-BTC-USD"]}
            # Server is sending Minute charts changes and some more
            # info available on public page
            # https://cex.io/ohlcv/btc/usd
            # Periods acceptable: 1m 3m 5m 15m 30m 1h 2h 4h 6h 12h 1d 3d 1w
            ({'e': 'ohlcv-init-new', }, on_ohlcv_init_new),
            ({'e': 'ohlcv-new', }, on_ohlcv_new),
            ({'e': 'ohlcv1m', }, on_ohlcv1m),

            # Send: {"e": "subscribe", "rooms": ["pair-BTC-USD"]}
            # Server is sendig Trading history and updates, Order Book (MD)
            # and Market Depth (MD-grouped)
            ({'e': 'md', }, on_md),
            ({'e': 'md_groupped', }, on_md_grouped),
            ({'e': 'history', }, on_history),
            ({'e': 'history-update', }, on_history_update),
            ({'e': 'ohlcv', }, on_ohlcv),
            ({'e': 'ohlcv24', }, on_ohlcv24),

        )) + sink

    async def on_notification(self, message):
        return await self.n_router(message)


async def write_bid():
    await aio.sleep(3)
    logger.debug('writing bid')
    logger.debug('writing bid')


async def write_ask():
    await aio.sleep(10)
    logger.debug('writing ask')
    logger.debug('writing ask')


async def cexiosocket_to_pg(dct: dict) -> None:
    """
    Function writes to postgres server

    :param dct: pass in the dictionary from the websocket
    :return: None but writes to postgres
    """

    logger.debug('\n%%%received data:')
    logger.debug(dct)

    df_ask = pd.DataFrame(columns=['price', 'quantity'],
                          data=dct.get('sell'))

    df_bid = pd.DataFrame(columns=['price', 'quantity'],
                          data=dct.get('buy'))

    md_time, md_id = int(datetime.datetime.utcnow().strftime("%s")),\
        dct.get('id')

    df_ask['book_id'], df_ask['server_time'], df_ask['my_time'] = \
        md_id, dct.get(''), md_time

    df_bid['book_id'], df_bid['server_time'], df_bid['my_time'] = \
        md_id, dct.get(''), md_time

    df_bid['session_id'], df_ask['session_id'] = 4, 4

    logger.debug('bid')
    logger.debug(df_bid)
    logger.debug('ask')
    logger.debug(df_ask)

    logger.debug(
        "Importing for id {} and time {}".format(dct.get('id'), md_time))

    try:
        aio.ensure_future(async_insert_df(df=df_bid, table='LTwo.Bid'))
        aio.ensure_future(async_insert_df(df=df_ask, table='LTwo.Ask'))
    except pg.DatabaseError as e:
        logger.error(e)


def main():

    # to test reconnecting
    async def _force_disconnect():
        while True:
            try:
                await aio.sleep(random.randrange(24, 64))
                print('test > Force disconnect')
                await client.ws.close()
            except Exception as exc:
                print("Exception at closing connection: {}".format(exc))

    config = {
        'ws': {
            'uri': 'wss://ws.cex.io/ws/',
        },
        'rest': {
            'uri': 'https://cex.io/api/',
        },
        'authorize': False,
        'auth': {
            'user_id': 'up128298374',
            'key': 'QAdqiA6mE1W2i9JQlAC6ycfa3i4',
            'secret': 'xPA89ud7pha6XRZudOpGpppg',
        },
    }

    client = WebSocketClientPublicData(config)

    async def public_subscription_test():
        # await client.send_subscribe({'e':
        #   'subscribe', 'rooms': ['tickers', ],})
        # await client.send_subscribe({"e": "init-ohlcv-new","i": "1m",
        #   "rooms": ["pair-BTC-USD"]})
        await client.send_subscribe({"e": "subscribe",
                                     "rooms": ["pair-BTC-USD"]})
        # await client.send_subscribe(
        #     {"e": "order-book-subscribe",
        #      "data": {"pair": ["BTC", "USD"],
        #               "subscribe": "false",
        #               "depth": -1}})

    try:
        loop = aio.get_event_loop()
        loop.set_debug(True)
        loop.run_until_complete(client.run())
        # aio.ensure_future(_force_disconnect()),
        loop.run_until_complete(public_subscription_test())
        loop.run_forever()
        loop.close()
    except Exception as exc:
        print("Exception at exit: {}".format(exc))


if __name__ == '__main__':
    main()
