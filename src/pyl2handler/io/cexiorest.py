"""
Base on `Cex.io project <https://github.com/cexioltd/CEX.IO-Client-Python3.5>`_
"""

import asyncio
import datetime
import hashlib
import hmac
import logging as lg
import logging.config
import time
import psycopg2
from datetime import datetime as dt

import aiohttp
import pandas as pd
from pyl2handler.helpers import get_config, get_path
from pyl2handler.io.pg import insert_df


logging.config.fileConfig(get_path(('logging_config.ini',), True))
logger = lg.getLogger('simpleExample')


# Assert that required algorithm implemented
assert 'sha256' in hashlib.algorithms_guaranteed


class ConfigError(Exception):
    """Any configuration error should be covered in this exception
    """
    print("Any configuration error should be covered in this exception")


class InvalidResponseError(Exception):
    """Indicates issue with REST resource requested by rest_client
    Is raised on eny not 'OK: 200' response
    """
    print("Indicates issue with REST resource requested by rest_client."
          "Is raised on eny not 'OK: 200' response")


class CEXRestAuth:
    """Creates the hash needed for authorisation
    """
    _json = {
        'key': None,
        'signature': None,
        'nonce': None,
    }

    def __init__(self, config):
        try:
            self._user_id = config['user_id']
            self._key = config['key']
            self._secret = config['secret']

        except KeyError as ex:
            raise ConfigError('Missing key in config file', ex)

    @staticmethod
    def get_curr_timestamp() -> int:
        """

        Returns:
            int: time stamp as an integer
        """
        return int(datetime.datetime.now().timestamp() * 1000)

    def get_timed_signature(self):
        """Returns java timestamp in miliseconds, and 'timed' signature,
        which is the digest of byte string, compound of timestamp,
        user ID ans public key
        """
        timestamp = self.get_curr_timestamp()
        message = "{}{}{}".format(timestamp, self._user_id, self._key)
        return timestamp, hmac.new(self._secret.encode(), message.encode(),
                                   hashlib.sha256).hexdigest()

    # noinspection PyTypeChecker
    def get_params(self):
        """
        Returns JSON from self._json dict
        The request is valid within ~20 seconds
        """
        timestamp, signature = self.get_timed_signature()
        self._json['key'] = self._key
        self._json['signature'] = signature
        self._json['nonce'] = timestamp
        return self._json


class CEXRestClient:
    """Handles the REST API calls.
    """

    def __init__(self, config: dict) -> None:

        self._conf = config
        try:
            self._uri = config['rest']

        except KeyError as ex:
            raise ConfigError('Missing key in config file', ex)

    async def get(self, resource: str) -> dict:
        """
        Get API request

        Args:
            resource (str): pass in an API endpoint

        Returns:
            dict: returns dictionary with the data
        """
        url = self._uri + resource
        logger.debug("REST.Get> {}".format(url))

        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                self._validate(url, response)
                response = await response.json(content_type=None)
                logger.debug("REST.Resp> Response: {}".format(response))
                return response

    async def post(self, resource: str, params: dict = None,
                   auth: bool = False) -> dict:
        """
        Post API request

        Args:
            resource (str):  pass in an API endpoint
            params (dict): dictionary with parameter
            auth (bool): pass in (True) if the request is private

        Returns:
            dict: returns dictionary with the data
        """
        if params is None:
            params = {}
        url = self._uri + resource
        logger.debug("REST.Post> {}".format(url))

        if auth:
            dct = CEXRestAuth(self._conf).get_params()
            params.update(key=dct['key'],
                          signature=dct['signature'],
                          nonce=dct['nonce'])

        async with aiohttp.ClientSession() as session:
            async with session.post(url, data=params) as response:
                self._validate(url, response)
                response = await response.json(content_type=None)
                logger.debug("REST.Resp> {}".format(response))
                return response

    @staticmethod
    def _validate(url: str, response) -> None:
        if response.status != 200:
            error = "Error response code {}: {} at: {}".format(
                response.status, response.reason, url)
            logger.debug(error)
            raise InvalidResponseError(error)

        content_type = response.headers['CONTENT-TYPE']
        if content_type != 'text/json':
            error = "Invalid response content-type \'{}\' of: {}".format(
                content_type, url)
            logger.debug(error)
            raise InvalidResponseError(error)


def cexrest_to_pg():
    """
    Collect the data from Cex IO and stores it to the Postgres Server.
    This is obsolete. DO NOT USE!

    Returns: None
    """
    loop = asyncio.get_event_loop()
    dct_track = {}
    try:
        for i in range(6000000):
            conf = get_config(section='cexio_home')
            client = CEXRestClient(conf)
            dct = loop.run_until_complete(
                client.post("order_book/BTC/USD", {'depth': ''}))
            id = dct.get('id')
            if id in dct_track:
                print(id)
                print(dct_track)
                time.sleep(0.3)
                continue
            else:
                print(id)
                print("i: " + str(i))
                dct_track[id] = None
                df_ask = pd.DataFrame(columns=['price', 'quantity'],
                                      data=dct.get('asks'))

                df_ask = df_ask.groupby(by=['price'], as_index=False).sum()

                df_ask['book_id'], df_ask['server_time'], df_ask['my_time'] =\
                    dct.get('id'), dct.get('timestamp'),\
                    int(dt.utcnow().strftime("%s"))

                df_bid = pd.DataFrame(columns=['price', 'quantity'],
                                      data=dct.get('bids'))

                df_bid = df_bid.groupby(by=['price'], as_index=False).sum()

                df_bid['book_id'], df_bid['server_time'], df_bid['my_time'] = \
                    dct.get('id'), dct.get('timestamp'),\
                    int(dt.utcnow().strftime("%s"))

                try:
                    insert_df(df=df_bid, table='LTwo.Ask')
                    insert_df(df=df_ask, table='LTwo.Bid')
                except psycopg2.DatabaseError:
                    logger.error('Error on DB')
                    continue
    finally:
        loop.close()
        del loop
