"""
see `some tutorials <http://www.postgresqltutorial.com/postgresql-python/>`_
"""
import logging as lg
import logging.config

import pandas as pd
import psycopg2
import psycopg2.extras as ex
from pyl2handler.helpers import get_config, get_path

logging.config.fileConfig(get_path(('logging_config.ini',), True))
logger = lg.getLogger('simpleExample')


def get_version(filename: tuple = ('io', 'credentials.ini')):
    """
    This function is for testing. If you get the version number using this
    function, the connection works fine.

    Args:
        filename (tuple): file name for the credentials

    Returns:
        None
    """
    conn = None
    try:
        # read connection parameters
        params = get_config(filename=filename)

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        raise error
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


def insert_df(df: pd.DataFrame(), table: str,
              filename: tuple = ('io', 'credentials.ini')):
    conn = None
    try:
        insert_stmt = "INSERT INTO {} ({}) {}".format(
            table, ", ".join(df.columns.to_list()),
            "VALUES(" + ("%s," * len(df.columns))[:-1] + ")")

        params = get_config(filename=filename)
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        ex.execute_batch(cur, insert_stmt, df.values, page_size=1000)

        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        raise error
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed. Import completed')


async def async_insert_df(df: pd.DataFrame(), table: str,
                          filename: tuple = ('io', 'credentials.ini')):
    conn = None
    try:
        insert_stmt = "INSERT INTO {} ({}) {}".format(
            table, ", ".join(df.columns.to_list()),
            "VALUES(" + ("%s," * len(df.columns))[:-1] + ")")

        params = get_config(filename=filename)
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        ex.execute_batch(cur, insert_stmt, df.values, page_size=1000)

        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        raise error
    finally:
        if conn is not None:
            conn.close()
            logger.debug('Imported the df for table {}'.format(table))


def sql_to_df(sql: str, filename: tuple = ('io', 'credentials.ini')):
    """
    Returns pd.DataFrame based on the SQL string.
    Args:
        sql (str): SQL string
        filename (tuple): file name for the credentials

    Returns:
        pd.DataFrame
    """
    conn = None
    try:
        params = get_config(filename=filename)
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql)
        dct = cur.fetchall()
        df = pd.DataFrame(dct, columns=[headers[0]
                                        for headers in cur.description])
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        raise error
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed. Read Completed')
    return df
