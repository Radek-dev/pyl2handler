import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pmdarima as pm
import seaborn as sns
import statsmodels.api as sm
from statsmodels.tsa.vector_ar.vecm import VECM, VAR
from pyl2handler.helpers import store_queries as qry
from pyl2handler.analysis.stats_tests import grangers_causation_matrix
from pyl2handler.io.pg import sql_to_df
import matplotlib.dates as mdate
import scipy.optimize as sco
from random import sample, seed
from .stats_tests import PerformStationarityTests

mpl.use('Qt5Agg')
sns.set()
figures = '/home/rad/projects/l2handler/Thesis/Figures/'
listings = '/home/rad/projects/l2handler/Thesis/Listings/'
tables = '/home/rad/projects/l2handler/Thesis/Tables/'
save_file = 0


def models_for_volume_init_analysis() -> None:
    sql_ask, sql_bid = qry('basic_sorted')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    if 'number_of_paths' not in locals():
        number_of_paths = 50

    df_ask.sort_values(['book_id', 'price'], inplace=True)
    df_ask['my_time'] = pd.to_datetime(df_ask['my_time'].values)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins = \
        pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                     number_of_paths), number_of_paths),
                     columns=['MinAsk' + str(i + 1) for i in
                              range(number_of_paths)])
    df_mins.index = \
        pd.to_datetime(df_ask.groupby(['book_id']).min()['my_time'].values)
    df_mins['my_time'] = pd.to_datetime(df_ask.groupby(['book_id']).min()['my_time'].values)
    df_mins = df_mins[::30]

    df_mins.info()
    df_ask.info()

    df_v_a = pd.DataFrame(index=pd.date_range(df_mins.index[0],
        periods=df_mins.shape[0], freq='T'))

    # this needs a loop to complete for every column
    for i in range(1, 5):
        df_v_a['qty_ask' + str(i)] =\
            pd.merge(df_mins, df_ask, how='inner',
                     left_on=['MinAsk' + str(i), 'my_time'],
                     right_on=['price', 'my_time'])[['quantity']].values

    df_v_a['qty_ask3'].plot()
    plt.show()

    df_mins.index = pd.date_range(df_mins.index[0], periods=df_mins.shape[0],
                                  freq='T')
    del df_ask


def working_on_volume_sample2():
    """
        Finding volume models for larger data set
    """
    df_ask = sql_to_df(qry('basic_sorted4')[0])
    prices = sorted(df_ask['price'].unique())
    df_v_a = pd.DataFrame(columns=prices,
                          index=pd.to_datetime(
                              sorted(df_ask['my_time'].unique())))

    for price in prices:
        df = df_ask[df_ask['price'] == price][['quantity', 'my_time']]
        df.rename(columns={'quantity': price}, inplace=True)
        df_v_a.loc[df['my_time'], price] = df[price].values
    del price

    df_v_a.index = df_v_a.index.values
    df_v_a.index =\
        pd.date_range(df_v_a.index[0], periods=df_v_a.shape[0], freq='T')
    df_v_a.replace(np.nan, 0, inplace=True)
    df_v_a = df_v_a.cumsum(axis=1)
    df_v_a = df_v_a[::30]

    for i in df_v_a.columns:
        print(i)

    del df_ask

    if save_file:
        seed(10)
        sns.set(font_scale=2)
        df_v_a[sample(set(df_v_a.columns), 10)].plot(marker='.',
                                                     figsize=(15, 10))
        plt.tight_layout()
        plt.ylabel('Cumulative Quantities for Some Ask Prices')
        plt.savefig(figures + 'cum_qty_over_time.png', dpi=300)
    else:
        plt.show()

    if save_file:
        df_v_a[11826.1].plot(marker='.', figsize=(15, 10), legend=True)
        plt.tight_layout()
        plt.ylabel('Cumulative Quantities for 11826.1 USC/BTC')
        plt.savefig(figures + 'cum_qty_over_one_price.png', dpi=300)
    else:
        plt.show()

    # s_test = PerformStationarityTests(df_v_a[11826.1])
    # s_test.show_results(print_results=True)

    model = \
        sm.tsa.MarkovAutoregression(df_v_a[11826.1],
                                    k_regimes=2, order=2,
                                    switching_ar=False)
    fit = model.fit()
    if save_file:
        print(fit.summary(),
              file=open(listings + 'markov_switching_good.txt', 'a'))
        fit.filtered_marginal_probabilities[0].plot(figsize=(15, 10),
                                                    xticks=[])
        plt.tight_layout()
        plt.savefig(figures + 'markov_qty_p00.png', dpi=300)
        plt.show()
    else:
        fit.summary()

    fit.smoothed_marginal_probabilities[0].plot(figsize=(15, 10))
    plt.show()

    fit.filtered_marginal_probabilities[0].plot(figsize=(15, 10))
    plt.show()

    model =\
        sm.tsa.MarkovAutoregression(df_v_a[11826.1],
                                    k_regimes=2, order=4,
                                    switching_ar=False)
    fit = model.fit()
    fit.summary()

    model = \
        sm.tsa.MarkovAutoregression(df_v_a[11826.1],
                                    k_regimes=4, order=2,
                                    switching_ar=False)
    fit = model.fit()
    fit.summary()

