import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pmdarima as pm
import seaborn as sns
import statsmodels.api as sm
from statsmodels.tsa.vector_ar.vecm import VECM, VAR
from pyl2handler.helpers import store_queries as qry
from pyl2handler.analysis.stats_tests import grangers_causation_matrix
from pyl2handler.io.pg import sql_to_df
import matplotlib.dates as mdate
import scipy.optimize as sco


mpl.use('Qt5Agg')
sns.set()
figures = '/home/rad/projects/l2handler/Thesis/Figures/'
listings = '/home/rad/projects/l2handler/Thesis/Listings/'
tables = '/home/rad/projects/l2handler/Thesis/Tables/'
save_file = 0


def find_arima() -> None:
    """
    Find ARIMA fit for bid-ask spread. Plots the residual diagnostics.
    """

    sql_ask, sql_bid = qry('basic')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)
    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]
    df_bid_max = df_bid.groupby(by=['book_id']).max()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)
    df_bid_max.sort_values(by=['my_time'], inplace=True)

    df_spread = pd.DataFrame(
        data=(df_ask_min['price'] - df_bid_max['price']).values,
        index=df_ask_min['my_time'], columns=['spread'])

    stepwise_fit = pm.auto_arima(df_spread['spread'], start_p=1, start_q=1,
                                 max_p=6, max_q=6, m=1, seasonal=False,
                                 d=0, trace=True, error_action='warn',
                                 suppress_warnings=True, stepwise=True)

    stepwise_fit.plot_diagnostics(figsize=(15, 10))
    plt.tight_layout()
    sns.set(font_scale=1.6)
    if save_file:
        print(stepwise_fit.summary(),
              file=open(listings + 'arima_results.txt', 'a'))
        plt.savefig(figures + 'arima_residuals.png', dpi=300)
    else:
        stepwise_fit.summary()
        plt.show()


def dual_linear_regression() -> None:
    sql_ask, sql_bid = qry('one_point')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask.sort_values('price', inplace=True)
    df_ask['cum_quantity_ask'] = df_ask['quantity'].cumsum()
    df_ask['const_ask'] = 1.0

    df_bid.sort_values('price', ascending=False, inplace=True)
    df_bid['cum_quantity_bid'] = df_bid['quantity'].cumsum()
    df_bid['const_bid'] = 1.0

    fit_ask = sm.OLS(df_ask['price'],
                     df_ask[['cum_quantity_ask', 'const_ask']]).fit()
    fit_ask.summary()

    fit_bid = sm.OLS(df_bid['price'],
                     df_bid[['cum_quantity_bid', 'const_bid']]).fit()
    fit_bid.summary()

    lin_space = np.arange(-1, 16)

    plt.figure(figsize=(15, 10))
    sns.set(font_scale=2)
    plt.rc('text', usetex=True)
    plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']})
    plt.scatter(df_ask['cum_quantity_ask'], df_ask['price'], c='blue',
                label=r'Price $P$ for given $A(P)$', alpha=0.5)
    plt.scatter(df_bid['cum_quantity_bid'], df_bid['price'], c='orange',
                label=r'Price $P$ for given $B(P)$', alpha=0.5)
    plt.plot(lin_space,
             fit_ask.params.const_ask +
             fit_ask.params.cum_quantity_ask * lin_space,
             label=r'Linear Fit Based on $A(P)$ ')

    plt.plot(lin_space,
             fit_bid.params.const_bid +
             fit_bid.params.cum_quantity_bid * lin_space,
             label=r'Linear Fit Based on $B(P)$')
    plt.ylabel(r'$P$', rotation='horizontal')
    plt.xlabel(r'$Q$')
    plt.legend()
    plt.tight_layout()
    if save_file:
        print(fit_ask.summary(),
              file=open(listings + 'linear_regression_ask.txt', 'a'))
        print(fit_bid.summary(),
              file=open(listings + 'linear_regression_bid.txt', 'a'))
        plt.savefig(figures + 'two_linear_regressions.png', dpi=300)
    else:
        fit_ask.summary()
        fit_bid.summary()
        plt.show()


def dual_linear_regression_with_time() -> None:
    sql_ask, sql_bid = qry('basic_sorted')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask['const_ask'], df_bid['const_bid'] = 1, 1

    for book_id in np.sort(df_ask['book_id'].unique()):
        print(book_id)

    ls_intercept = []
    for book_id in np.sort(df_ask['book_id'].unique()):
        print("\n\n" + str(book_id))
        ask_slice = df_ask[df_ask['book_id'] == book_id][
            ['price', 'quantity', 'const_ask']].sort_values('price')
        ask_slice['cum_quantity_ask'] = ask_slice['quantity'].cumsum()

        bid_slice = df_bid[df_bid['book_id'] == book_id][
            ['price', 'quantity', 'const_bid']].sort_values('price',
                                                            ascending=False)
        bid_slice['cum_quantity_bid'] = bid_slice['quantity'].cumsum()

        fit_ask = sm.OLS(ask_slice['price'],
                         ask_slice[['cum_quantity_ask', 'const_ask']]).fit()

        fit_bid = sm.OLS(bid_slice['price'],
                         bid_slice[['cum_quantity_bid', 'const_bid']]).fit()

        print(fit_ask.summary())
        print(fit_bid.summary())

        intercept = \
            (fit_ask.params['const_ask'] * fit_bid.params['cum_quantity_bid'] -
             fit_ask.params['cum_quantity_ask'] * fit_bid.params['const_bid'])\
            / (fit_bid.params['cum_quantity_bid']
               - fit_ask.params['cum_quantity_ask'])

        print("\n" + str(intercept))
        ls_intercept.append(intercept)

    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]
    df_bid_max = df_bid.groupby(by=['book_id']).max()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)
    df_bid_max.sort_values(by=['my_time'], inplace=True)

    df_bid_max['Peq'] = ls_intercept

    fig, ax = plt.subplots()
    plt.plot(df_ask_min['my_time'], df_ask_min['price'],
             marker='.', label='Ask Price')
    plt.plot(df_bid_max['my_time'], df_bid_max['price'],
             marker='.', label='Bid Price')
    plt.plot(df_ask_min['my_time'], ls_intercept,
             marker='.', label='Equilibrium')
    plt.legend()
    ax.set_ylabel('1 BTC in USD')
    ax.set_xlabel('Time on 28th June 2020')
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
    fig.autofmt_xdate()
    plt.tight_layout()
    if save_file:
        fig.savefig(figures + 'estimated_equilibrium.png', dpi=300)
    else:
        plt.show()


def dual_linear_regression_interpretation() -> None:
    """
        Prepares the plot of two fitted linear curves
    """
    sql_ask, sql_bid = qry('basic_sorted')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_model1a = pd.DataFrame(df_ask[df_ask['book_id'] == 801551441])
    df_model1b = pd.DataFrame(df_bid[df_bid['book_id'] == 801551441])
    df_model2a = pd.DataFrame(df_ask[df_ask['book_id'] == 801551501])
    df_model2b = pd.DataFrame(df_bid[df_bid['book_id'] == 801551501])

    df_model1a.sort_values('price', inplace=False)
    df_model1a['cum_quantity_ask'] = df_model1a['quantity'].cumsum()
    df_model1a['const_ask'] = 1.0

    df_model2a.sort_values('price', inplace=True)
    df_model2a['cum_quantity_ask'] = df_model2a['quantity'].cumsum()
    df_model2a['const_ask'] = 1.0

    df_model1b.sort_values('price', ascending=False, inplace=True)
    df_model1b['cum_quantity_bid'] = df_model1b['quantity'].cumsum()
    df_model1b['const_bid'] = 1.0

    df_model2b.sort_values('price', ascending=False, inplace=True)
    df_model2b['cum_quantity_bid'] = df_model2b['quantity'].cumsum()
    df_model2b['const_bid'] = 1.0

    fit_ask1 = sm.OLS(df_model1a['price'],
                      df_model1a[['cum_quantity_ask', 'const_ask']]).fit()
    fit_ask1.summary()

    fit_bid1 = sm.OLS(df_model1b['price'],
                      df_model1b[['cum_quantity_bid', 'const_bid']]).fit()
    fit_bid1.summary()

    fit_ask2 = sm.OLS(df_model2a['price'],
                      df_model2a[['cum_quantity_ask', 'const_ask']]).fit()
    fit_ask2.summary()

    fit_bid2 = sm.OLS(df_model2b['price'],
                      df_model2b[['cum_quantity_bid', 'const_bid']]).fit()
    fit_bid2.summary()

    lin_space = np.arange(-1, 16)

    plt.figure(figsize=(15, 10))
    sns.set(font_scale=2.2)
    plt.rc('text', usetex=True)
    plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']})
    plt.scatter(df_model1a['cum_quantity_ask'], df_model1a['price'], c='blue',
                alpha=0.5)
    plt.scatter(df_model1b['cum_quantity_bid'], df_model1b['price'], c='blue',
                alpha=0.5)
    plt.plot(lin_space,
             fit_ask1.params.const_ask +
             fit_ask1.params.cum_quantity_ask * lin_space,
             label=r'Linear Fit Based on $A(P)_t$', c='blue')

    plt.plot(lin_space,
             fit_bid1.params.const_bid +
             fit_bid1.params.cum_quantity_bid * lin_space,
             label=r'Linear Fit Based on $B(P)_t$', c='blue')

    plt.scatter(df_model2a['cum_quantity_ask'], df_model2a['price'], c='orange',
                alpha=0.5)
    plt.scatter(df_model2b['cum_quantity_bid'], df_model2b['price'], c='orange',
                alpha=0.5)
    plt.plot(lin_space,
             fit_ask2.params.const_ask +
             fit_ask2.params.cum_quantity_ask * lin_space,
             label=r'Linear Fit Based on $A(P)_{t+z}$', c='orange')

    plt.plot(lin_space,
             fit_bid2.params.const_bid +
             fit_bid2.params.cum_quantity_bid * lin_space,
             label=r'Linear Fit Based on $B(P)_{t+z}$', c='orange')

    plt.ylabel(r'$P$', rotation='horizontal')
    plt.xlabel(r'$Q$')
    plt.legend()
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'two_linear_regressions_interpretation.png', dpi=300)
    else:
        plt.show()


def dual_linear_regression_polynomial_2() -> None:
    """
        Second degree polynomial - fit is not better
    """
    sql_ask, sql_bid = qry('one_point')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask.sort_values('price', inplace=True)
    df_ask['cum_quantity_ask'] = df_ask['quantity'].cumsum()
    df_ask['const_ask'] = 1.0
    df_ask['cum_quantity_ask^2'] = df_ask['cum_quantity_ask'] ** 2

    df_bid.sort_values('price', ascending=False, inplace=True)
    df_bid['cum_quantity_bid'] = df_bid['quantity'].cumsum()
    df_bid['const_bid'] = 1.0
    df_bid['cum_quantity_bid^2'] = df_bid['cum_quantity_bid'] ** 2

    fit_ask = sm.OLS(df_ask['price'], df_ask[
        ['cum_quantity_ask^2', 'cum_quantity_ask', 'const_ask']]).fit()
    fit_ask.summary()

    fit_bid = sm.OLS(df_bid['price'], df_bid[
        ['cum_quantity_bid^2', 'cum_quantity_bid', 'const_bid']]).fit()
    fit_bid.summary()

    lin_space = np.arange(-1, 16)
    lin_space2 = lin_space ** 2

    y_hat = fit_ask.params['const_ask'] + \
        fit_ask.params['cum_quantity_ask'] * lin_space + \
        fit_ask.params['cum_quantity_ask^2'] * lin_space2

    plt.scatter(df_ask['cum_quantity_ask'], df_ask['price'])
    plt.plot(lin_space, y_hat)

    print(fit_ask.summary(),
          file=open(listings + 'linear_regression_ask_polynomial.txt', 'a'))
    print(fit_bid.summary(),
          file=open(listings + 'linear_regression_bid_polynomial.txt', 'a'))

    plt.show()


def dual_linear_regression_polynomial_3() -> None:
    """
        Third degree polynomial - fit is not better
        this doesn't work
    """
    sql_ask, sql_bid = qry('one_point')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask.sort_values('price', inplace=True)
    df_ask['cum_quantity_ask'] = df_ask['quantity'].cumsum()
    df_ask['const_ask'] = 1.0
    df_ask['cum_quantity_ask^2'] = df_ask['cum_quantity_ask'] ** 2
    df_ask['cum_quantity_ask^3'] = df_ask['cum_quantity_ask'] ** 3

    df_bid.sort_values('price', ascending=False, inplace=True)
    df_bid['cum_quantity_bid'] = df_bid['quantity'].cumsum()
    df_bid['const_bid'] = 1.0
    df_bid['cum_quantity_bid^2'] = df_bid['cum_quantity_bid'] ** 2
    df_bid['cum_quantity_bid^3'] = df_bid['cum_quantity_bid'] ** 3

    fit_ask = sm.OLS(df_ask['price'], df_ask[
        ['cum_quantity_ask^3', 'cum_quantity_ask^2',
         'cum_quantity_ask', 'const_ask']]).fit()
    fit_ask.summary()

    fit_bid = sm.OLS(df_bid['price'], df_bid[
        ['cum_quantity_bid^3', 'cum_quantity_bid^2',
         'cum_quantity_bid', 'const_bid']]).fit()
    fit_bid.summary()

    print(fit_ask.summary(),
          file=open(listings + 'linear_regression_ask_polynomial3.txt', 'a'))
    print(fit_bid.summary(),
          file=open(listings + 'linear_regression_bid_polynomial3.txt', 'a'))


def dual_linear_regression_log_price() -> None:
    """
        Log transformation
    """
    sql_ask, sql_bid = qry('one_point')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask.sort_values('price', inplace=True)
    df_ask['cum_quantity_ask'] = df_ask['quantity'].cumsum()
    df_ask['const_ask'] = 1.0
    df_ask['price_log'] = np.log(df_ask['price'])

    df_bid.sort_values('price', ascending=False, inplace=True)
    df_bid['cum_quantity_bid'] = df_bid['quantity'].cumsum()
    df_bid['const_bid'] = 1.0
    df_bid['price_log'] = np.log(df_bid['price'])

    fit_ask = sm.OLS(df_ask['price_log'], df_ask[
        ['cum_quantity_ask', 'const_ask']]).fit()
    fit_ask.summary()

    fit_bid = sm.OLS(df_bid['price_log'], df_bid[
        ['cum_quantity_bid', 'const_bid']]).fit()
    fit_bid.summary()

    print(fit_ask.summary(),
          file=open(listings + 'log_linear_regression.txt', 'a'))
    print(fit_bid.summary(),
          file=open(listings + 'log_linear_regression_bid.txt', 'a'))

    lin_space = np.arange(-1, 16)

    y_hat = np.exp(fit_ask.params['const_ask'] +
                   fit_ask.params['cum_quantity_ask'] * lin_space)

    plt.scatter(df_ask['cum_quantity_ask'], df_ask['price'])
    plt.plot(lin_space, y_hat)
    plt.show()


def non_linear_fit_best_attempt():
    from lmfit import Model
    sql_ask, sql_bid = qry('one_point')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)
    df_ask.sort_values('price', inplace=True)
    df_bid.sort_values('price', inplace=True, ascending=False)

    df_ask['qty_usd_cum_log'] = \
        np.log((df_ask['price'] * df_ask['quantity']).cumsum())
    df_bid['qty_usd_cum_log'] = \
        np.log((df_ask['price'] * df_ask['quantity']).cumsum())

    mid_price = round((df_ask['price'].min() + df_bid['price'].max())/2, 2)

    df_ask['price_0'] = df_ask['price'] - mid_price
    df_bid['price_0'] = df_bid['price'] - mid_price

    sns.set(font_scale=2)
    plt.figure(figsize=(15, 10))
    plt.scatter(df_ask['price_0'], df_ask['qty_usd_cum_log'],
                label='Transformed Ask Price-Quantity Quote')
    plt.scatter(df_bid['price_0'], df_bid['qty_usd_cum_log'],
                label='Transformed Bid Price-Quantity Quote')
    plt.xlabel('Stabilised Quantity')
    plt.ylabel('Log Price')
    plt.legend()
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'transformed_problem.png', dpi=300)
    else:
        plt.show()

    def ask_curve(x, a0, a1, a2, a3):
        return a0 + a1 * x - a2 * np.exp(-a3 * x)

    def bid_curve(x, b0, b1, b2, b3):
        return b0 + b1 * x - b2 * np.exp(b3 * x)

    model_ask, model_bid = Model(ask_curve), Model(bid_curve)

    result_lsq_ask = \
        model_ask.fit(df_ask['qty_usd_cum_log'].values,
                      x=df_ask['price_0'].values,
                      a0=9.51, a1=0.013, a2=5.6, a3=0.15, method='leastsq')
    result_lsq_ask.plot()
    if save_file:
        print(result_lsq_ask.fit_report(),
              file=open(listings + 'hockey_stick_ask.txt', 'a'))
        plt.savefig(figures + 'hockey_stick_ask.png', dpi=300)
    else:
        print(result_lsq_ask.fit_report())
        plt.show()
    a = result_lsq_ask.best_values

    result_lsq_bid = \
        model_bid.fit(df_bid['qty_usd_cum_log'].values,
                      x=df_bid['price_0'].values,
                      b0=9.38, b1=-0.01, b2=5.6, b3=0.15, method='leastsq')
    result_lsq_bid.plot()
    if save_file:
        print(result_lsq_bid.fit_report())
        print(result_lsq_bid.fit_report(),
              file=open(listings + 'hockey_stick_bid.txt', 'a'))
        plt.savefig(figures + 'hockey_stick_bid.png', dpi=300)
    else:
        plt.show()
    b = result_lsq_bid.best_values
    x = np.linspace(-2, 12, 200)
    xb = np.linspace(-2, 0.1, 200)

    plt.plot(df_bid['price_0'].values, bid_curve(df_bid['price_0'].values, b['b0'],b['b1'], b['b2'], b['b3']))
    plt.plot(xb, bid_curve(xb, b['b0'], b['b1'], b['b2'], b['b3']))
    plt.plot(df_ask['price_0'].values, ask_curve(df_ask['price_0'].values, a['a0'], a['a1'], a['a2'], a['a3']))
    plt.plot(x, ask_curve(x, a['a0'], a['a1'], a['a2'], a['a3']))
    plt.show()


def finding_var() -> None:
    """finding VAR model"""

    number_of_paths = 50

    df_ask, df_bid = sql_to_df(qry('basic_sorted4')[0]),\
                     sql_to_df(qry('basic_sorted4')[1])


    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins = \
        pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                     number_of_paths), number_of_paths),
                     columns=['MinAsk' + str(i + 1) for i in
                              range(number_of_paths)])

    df_mins.index = \
        pd.to_datetime(df_ask.groupby(['book_id']).min()['my_time'].values)

    df_mins = df_mins[::30]
    df_mins.index = pd.date_range(df_mins.index[0], periods=df_mins.shape[0],
                                  freq='T')
    del df_ask

    df_maxs = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_maxs = \
        pd.DataFrame(data=df_maxs.values.reshape(int(len(df_maxs) /
                     number_of_paths), number_of_paths),
                     columns=['MaxBid' + str(i + 1) for i in
                              range(number_of_paths)])
    df_maxs.index = \
        pd.to_datetime(df_bid.groupby(['book_id']).min()['my_time'].values)
    df_maxs = df_maxs[::30]
    df_maxs.index = pd.date_range(df_maxs.index[0], periods=df_mins.shape[0],
                                  freq='T')
    del df_bid

    # df = df_mins[
    #     ['MinAsk1', 'MinAsk2', 'MinAsk3', 'MinAsk4', 'MinAsk5', 'MinAsk6',
    #      'MinAsk7', 'MinAsk8', 'MinAsk9', 'MinAsk10', 'MinAsk11', 'MinAsk12',
    #      'MinAsk13', 'MinAsk14', 'MinAsk15', 'MinAsk16', 'MinAsk17',
    #      'MinAsk18', 'MinAsk19', 'MinAsk20', 'MinAsk21', 'MinAsk22',
    #      'MinAsk23', 'MinAsk24', 'MinAsk25']]

    gc_mins = grangers_causation_matrix(df_mins, variables=df_mins.columns)
    gc_masx = grangers_causation_matrix(df_maxs, variables=df_maxs.columns)

    gc_mins.to_latex(buf=tables + 'gc_ask.tex', index=True)
    gc_masx.to_latex(buf=tables + 'gc_bid.tex', index=True)
    gc_mins.to_csv(tables + 'gc_ask.csv', index=True)
    gc_masx.to_csv(tables + 'gc_bid.csv', index=True)

    df = df_mins[
        ['MinAsk1', 'MinAsk5', 'MinAsk6', 'MinAsk8',
         'MinAsk9', 'MinAsk10', 'MinAsk11', 'MinAsk21']]

    gc = grangers_causation_matrix(df, variables=df.columns)
    gc.to_latex(buf=tables + 'gc_small_ask.tex', index=True)
    nobs = 5
    df_train, df_test = df[0:-nobs], df[-nobs:]
    df_differenced = df_train.diff().dropna()


    # gc_words = np.where(gc < 0.05, 1, 0)
    # cointegration_test(df)
    # ADF Test on each column
    # for name, column in df_train.iteritems():
    #     adfuller_test(column, name=column.name)
    #     print('\n')
    # ADF Test on each column of 1st Differences Dataframe
    # for name, column in df_differenced.iteritems():
    #     adfuller_test(column, name=column.name)
    #     print('\n')

    model = VAR(df_differenced)
    x = model.select_order(maxlags=12, trend='nc')
    print(x.summary())
    if save_file:
        print(x.summary(),
              file=open(listings + 'var_order_selection.txt', 'a'))

    model_fitted = model.fit(maxlags=2, trend='nc')
    print(model_fitted.summary())
    if save_file:
        print(model_fitted.summary(),
              file=open(listings + 'var_result.txt', 'a'))

    from statsmodels.stats.stattools import durbin_watson
    out = durbin_watson(model_fitted.resid)

    def adjust(val, length=6):
        return str(val).ljust(length)
    for col, val in zip(df.columns, out):
        print(adjust(col), ':', round(val, 2))

    # Get the lag order
    lag_order = model_fitted.k_ar
    print(lag_order)

    # Input data for forecasting
    forecast_input = df_differenced.values[-lag_order:]
    forecast_input

    fc = model_fitted.forecast(y=forecast_input, steps=nobs)
    df_forecast = pd.DataFrame(fc, index=df.index[-nobs:],
                               columns=df.columns + '_1d')
    df_forecast

    def invert_transformation(df_train, df_forecast, second_diff=False):
        """Revert back the differencing to get the forecast to original scale."""
        df_fc = df_forecast.copy()
        columns = df_train.columns
        for col in columns:
            # Roll back 2nd Diff
            if second_diff:
                df_fc[str(col) + '_1d'] =\
                    (df_train[col].iloc[-1] - df_train[col].iloc[-2]) +\
                    df_fc[str(col) + '_2d'].cumsum()
            # Roll back 1st Diff
            df_fc[str(col) + '_forecast'] = df_train[col].iloc[-1] + df_fc[
                str(col) + '_1d'].cumsum()
        return df_fc

    df_results = invert_transformation(df_train, df_forecast)
    df_results.loc[:, df_results.columns.str.contains('fore')]

    # plot forecasts

    fig, axes = plt.subplots(nrows=int(len(df.columns) / 2), ncols=2, dpi=300,
                             figsize=(10, 10))
    for i, (col, ax) in enumerate(zip(df.columns, axes.flatten())):
        df_results[col + '_forecast'].plot(legend=True, ax=ax).autoscale(
            axis='x', tight=True)
        df_test[col][-nobs:].plot(legend=True, ax=ax);
        ax.set_title(col + ": Forecast vs Actuals")
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('none')
        ax.spines["top"].set_alpha(0)
        ax.tick_params(labelsize=6)

    plt.tight_layout();
    fig.savefig(figures + 'var_forecast.png', dpi=300)
    plt.show()

    # get model accuracy
    from statsmodels.tsa.stattools import acf
    def forecast_accuracy(forecast, actual):
        mape = np.mean(np.abs(forecast - actual) / np.abs(actual))  # MAPE
        me = np.mean(forecast - actual)  # ME
        mae = np.mean(np.abs(forecast - actual))  # MAE
        mpe = np.mean((forecast - actual) / actual)  # MPE
        rmse = np.mean((forecast - actual) ** 2) ** .5  # RMSE
        corr = np.corrcoef(forecast, actual)[0, 1]  # corr
        mins = np.amin(np.hstack([forecast[:, None],
                                  actual[:, None]]), axis=1)
        maxs = np.amax(np.hstack([forecast[:, None],
                                  actual[:, None]]), axis=1)
        minmax = 1 - np.mean(mins / maxs)  # minmax
        return ({'mape': mape, 'me': me, 'mae': mae,
                 'mpe': mpe, 'rmse': rmse, 'corr': corr, 'minmax': minmax})

    print('Forecast Accuracy of: MaxAsk1')
    accuracy_prod = forecast_accuracy(df_results['MinAsk1_forecast'].values,
                                      df_test['MinAsk1'])

    df_results['MinAsk1_forecast'].plot(legend=True)
    df_test['MinAsk1'].plot(legend=True)
    plt.show()

    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: pgnp')
    accuracy_prod = forecast_accuracy(df_results['pgnp_forecast'].values,
                                      df_test['pgnp'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: ulc')
    accuracy_prod = forecast_accuracy(df_results['ulc_forecast'].values,
                                      df_test['ulc'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: gdfco')
    accuracy_prod = forecast_accuracy(df_results['gdfco_forecast'].values,
                                      df_test['gdfco'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: gdf')
    accuracy_prod = forecast_accuracy(df_results['gdf_forecast'].values,
                                      df_test['gdf'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: gdfim')
    accuracy_prod = forecast_accuracy(df_results['gdfim_forecast'].values,
                                      df_test['gdfim'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: gdfcf')
    accuracy_prod = forecast_accuracy(df_results['gdfcf_forecast'].values,
                                      df_test['gdfcf'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))

    print('\nForecast Accuracy of: gdfce')
    accuracy_prod = forecast_accuracy(df_results['gdfce_forecast'].values,
                                      df_test['gdfce'])
    for k, v in accuracy_prod.items():
        print(adjust(k), ': ', round(v, 4))


def finding_vecm():
    """
        find VECM models and plots
    """
    if 'number_of_paths' not in locals():
        number_of_paths = 23

    sql_ask, sql_bid = qry('basic_sorted4')

    df_ask = sql_to_df(sql_ask)
    df_bid = sql_to_df(sql_bid)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins = \
        pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                     number_of_paths), number_of_paths),
                     columns=['MinAsk' + str(i + 1) for i in
                              range(number_of_paths)])

    df_mins.index = \
        pd.to_datetime(df_ask.groupby(['book_id']).min()['my_time'].values)

    df_mins = df_mins[::30]
    df_mins.index = pd.date_range(df_mins.index[0], periods=df_mins.shape[0],
                                  freq='T')
    del df_ask

    df_maxs = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_maxs = \
        pd.DataFrame(data=df_maxs.values.reshape(int(len(df_maxs) /
                     number_of_paths), number_of_paths),
                     columns=['MaxBid' + str(i + 1) for i in
                              range(number_of_paths)])
    df_maxs.index = \
        pd.to_datetime(df_bid.groupby(['book_id']).min()['my_time'].values)
    df_maxs = df_maxs[::30]
    df_maxs.index = pd.date_range(df_maxs.index[0], periods=df_mins.shape[0],
                                  freq='T')
    del df_bid


    fig, ((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)

    # data = df_mins[
    #     ['MinAsk1', 'MinAsk2', 'MinAsk3', 'MinAsk4', 'MinAsk5', 'MinAsk6',
    #      'MinAsk7', 'MinAsk8', 'MinAsk9', 'MinAsk10', 'MinAsk11', 'MinAsk12',
    #      'MinAsk13', 'MinAsk14', 'MinAsk15', 'MinAsk16', 'MinAsk17',
    #      'MinAsk18', 'MinAsk19', 'MinAsk20', 'MinAsk21', 'MinAsk22',
    #      'MinAsk23', 'MinAsk24', 'MinAsk25', 'MinAsk26', 'MinAsk27',
    #      'MinAsk28', 'MinAsk29', 'MinAsk30', 'MinAsk31', 'MinAsk32',
    #      'MinAsk33', 'MinAsk34', 'MinAsk35', 'MinAsk36', 'MinAsk37',
    #      'MinAsk38', 'MinAsk39', 'MinAsk40', 'MinAsk41', 'MinAsk42',
    #      'MinAsk43', 'MinAsk44', 'MinAsk45', 'MinAsk46', 'MinAsk47',
    #      'MinAsk48', 'MinAsk49', 'MinAsk50']]
    # df = df_mins[['MinAsk25', 'MinAsk26', 'MinAsk32', 'MinAsk35', 'MinAsk36',
    #               'MinAsk37', 'MinAsk39', 'MinAsk41', 'MinAsk42', 'MinAsk46',
    #               'MinAsk47', 'MinAsk50']]


    df = df_mins[['MinAsk2', 'MinAsk7', 'MinAsk11',
                    'MinAsk17', 'MinAsk18', 'MinAsk23']]
    data = df.diff().dropna()
    sns.set(font_scale=2)
    df.plot(marker='.', legend=True, title='Cointegrated Ask Prices')
    plt.tight_layout()
    plt.savefig(figures + 'vecm.png', dpi=300)
    plt.show()
    res = VECM(data, coint_rank=1, k_ar_diff=1, deterministic='nc', freq='T').fit()
    res.summary()
    res.plot_forecast(10)
    plt.show()


def finding_arima_for_price() -> None:
    """
        ARIMA for price
    """
    if 'number_of_paths' not in locals():
        number_of_paths = 20

    sql_ask, sql_bid = qry('basic_sorted4')

    df_ask = sql_to_df(sql_ask)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins = \
        pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                     number_of_paths), number_of_paths),
                     columns=['MinAsk' + str(i + 1) for i in
                              range(number_of_paths)])

    df_mins.index = \
        pd.to_datetime(df_ask.groupby(['book_id']).min()['my_time'].values)

    df_mins = df_mins[::30]
    df_mins.index = pd.date_range(df_mins.index[0], periods=df_mins.shape[0],
                                  freq='T')
    del df_ask

    stepwise_fit = pm.auto_arima(df_mins['MinAsk5'], start_p=1, start_q=1,
                                 max_p=10, max_q=10, m=1, seasonal=False,
                                 d=1, trace=True, error_action='warn',
                                 suppress_warnings=True, stepwise=True,
                                 with_intercept=False)

    stepwise_fit.plot_diagnostics(figsize=(15, 10))
    plt.tight_layout()
    sns.set(font_scale=1.6)
    if save_file:
        print(stepwise_fit.summary(),
              file=open(listings + 'arima_price.txt', 'a'))
        plt.savefig(figures + 'arima_price.png', dpi=300)
    else:
        stepwise_fit.summary()
        plt.show()


if __name__ == '__main__':
    find_arima()
