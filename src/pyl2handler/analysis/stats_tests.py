import numpy as np
import pandas as pd
from pyl2handler.helpers import store_queries as qry
from pyl2handler.io.pg import sql_to_df
from statsmodels.tsa.stattools import adfuller, grangercausalitytests
from statsmodels.tsa.vector_ar.vecm import coint_johansen


tables = '/home/rad/projects/l2handler/Thesis/Tables/'


class PerformStationarityTests:
    def __init__(self, ts=[], significance_level=.05):
        """
        Pass in a list like object to perform the ADF test. Use the
        show_results method to see the results.

        Args:
            ts: list, np.array or pd.series - anything that adfuller from
                statsmodels can handle
            significance_level: change the level of significance
        """
        self.significance_level = significance_level
        self.p_value = None
        self.adf_test = None
        self.s_results = None
        self.adf_test = adfuller(ts, autolag='AIC', regression='ct')
        self.p_value = self.adf_test[1]

        self.s_results = pd.Series(self.adf_test[0:4],
                                   index=['ADF Test Statistic', 'P-Value',
                                          '# Lags Used',
                                          '# Observations Used'])

        # Add Critical Values
        for key, value in self.adf_test[4].items():
            self.s_results['Critical Value (%s)' % key] = value

        self.s_results['Is stationary'] = \
            1 if self.p_value < self.significance_level else 0

    def show_results(self, print_results: bool = False,
                     tabulate_results: bool = False):

        if print_results:
            print('Augmented Dickey-Fuller Test Results:')
            print(self.s_results)

        if tabulate_results:
            self.s_results.to_latex(buf=tables + 'adf_test_qt.tex', index=True,
                                    header=['Value'])


def grangers_causation_matrix(data, variables, test='ssr_chi2test',
                              verbose=False, maxlag=12):
    """Check Granger Causality of all possible combinations of the Time series.
    The rows are the response variable, columns are predictors. The values in
    the table are the P-Values. P-Values lesser than the significance level
    (0.05), implies the Null Hypothesis that the coefficients of the
    corresponding past values is zero, that is, the X does not cause Y can
    be rejected.

    data      : pandas dataframe containing the time series variables
    variables : list containing names of the time series variables.

    based on: https://cutt.ly/jfkfpwW
    """
    df = pd.DataFrame(np.zeros((len(variables), len(variables))),
                      columns=variables, index=variables)
    for c in df.columns:
        for r in df.index:
            test_result = grangercausalitytests(data[[r, c]], maxlag=maxlag,
                                                verbose=False)
            p_values = [round(test_result[i + 1][0][test][1], 4) for i in
                        range(maxlag)]
            if verbose:
                print(f'Y = {r}, X = {c}, P Values = {p_values}')
            min_p_value = np.min(p_values)
            df.loc[r, c] = min_p_value
    df.columns = [var + '_x' for var in variables]
    df.index = [var + '_y' for var in variables]
    return df


def cointegration_test(df, alpha=0.05):
    """
        Perform Johanson's Cointegration Test and Report Summary
        based on: https://cutt.ly/jfkfpwW
    """
    out = coint_johansen(df, -1, 5)
    d = {'0.90': 0, '0.95': 1, '0.99': 2}
    traces = out.lr1
    cvts = out.cvt[:, d[str(1 - alpha)]]

    def adjust(val, length=6): return str(val).ljust(length)

    # Summary
    print('Name   ::  Test Stat > C(95%)    =>   Signif  \n', '--' * 20)
    for col, trace, cvt in zip(df.columns, traces, cvts):
        print(adjust(col), ':: ', adjust(round(trace, 2), 9), ">",
              adjust(cvt, 8), ' =>  ', trace > cvt)


def adfuller_test(series, signif=0.05, name='', verbose=False):
    """
        Perform ADFuller to test for Stationarity of given
        series and print report
        based on: https://cutt.ly/jfkfpwW
    """
    r = adfuller(series, autolag='AIC')
    output = {'test_statistic': round(r[0], 4), 'pvalue': round(r[1], 4),
              'n_lags': round(r[2], 4), 'n_obs': r[3]}
    p_value = output['pvalue']

    def adjust(val, length=6):
        return str(val).ljust(length)

    # Print Summary
    print(f'    Augmented Dickey-Fuller Test on "{name}"', "\n   ",
          '-' * 47)
    print(f' Null Hypothesis: Data has unit root. Non-Stationary.')
    print(f' Significance Level    = {signif}')
    print(f' Test Statistic        = {output["test_statistic"]}')
    print(f' No. Lags Chosen       = {output["n_lags"]}')

    for key, val in r[4].items():
         print(f' Critical value {adjust(key)} = {round(val, 3)}')

    if p_value <= signif:
        print(f" => P-Value = {p_value}. Rejecting Null Hypothesis.")
        print(f" => Series is Stationary.")
    else:
        print(
            f" => P-Value = {p_value}. Weak evidence to reject the Null Hypothesis.")
        print(f" => Series is Non-Stationary.")


def perform_adf_test():

    sql_ask, sql_bid = qry('basic')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)
    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]
    df_bid_max = df_bid.groupby(by=['book_id']).max()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)
    df_bid_max.sort_values(by=['my_time'], inplace=True)

    df_spread = pd.DataFrame(
        data=(df_ask_min['price'] - df_bid_max['price']).values,
        index=df_ask_min['my_time'], columns=['spread'])

    s_test = PerformStationarityTests(df_spread)
    s_test.show_results(print_results=True)


if __name__ == '__main__':
    perform_adf_test()
