from statsmodels.tsa.vector_ar.vecm import coint_johansen
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

mpl.use('Qt5Agg')

df = pd.read_csv("AirQualityUCI.csv", sep=";", parse_dates=[['Date', 'Time']],
                 decimal=",")
df.dropna(axis=1, how='all', inplace=True)
df.index = pd.to_datetime(df['Date_Time'], format='%d/%m/%Y %H.%M.%S')
df.drop(['Date_Time'], axis=1, inplace=True)
df.replace(to_replace=-200, value=np.nan, inplace=True)
df.interpolate(method='linear', limit_direction='forward', inplace=True)


# checking stationarity
# since the test works for only 12 variables, I have randomly dropped
# in the next iteration, I would drop another and check the eigenvalues
johan_test_temp = df.drop(['CO(GT)'], axis=1)
coint_johansen(johan_test_temp, -1, 1).eig

train = df[:int(0.8*(len(df)))]
valid = df[int(0.8*(len(df))):]

from statsmodels.tsa.vector_ar.var_model import VAR

model = VAR(endog=train, freq=train.index.inferred_freq)
model_fit = model.fit(1)
model_fit.summary()

# make prediction on validation
prediction = pd.DataFrame(data=model_fit.forecast(model_fit.y,
                          steps=len(valid)), columns=df.columns)





