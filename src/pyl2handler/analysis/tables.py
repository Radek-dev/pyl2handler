import pandas as pd
from pyl2handler.helpers import store_queries as qry
from pyl2handler.io.pg import sql_to_df

tables = '/home/rad/projects/l2handler/Thesis/Tables/'


def table_data_ask_ask() -> None:
    """
        Prepare the data table for ask and ask quotes
    """

    sql_ask, sql_bid = qry('data_table_ask'), qry('data_table_bid')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask.to_latex(buf=tables + 'data_ask.tex', index=True)
    df_bid.to_latex(buf=tables + 'data_bid.tex', index=True)


def table_sample_summary() -> None:
    """
        Prepares the tables summarising sample
    """
    sql_ask = qry('basic_sorted')[0]

    df_ask = sql_to_df(sql_ask)

    df_ask.groupby('book_id').count()

    df = pd.DataFrame(columns=['Definition', 'Value'])

    number_of_iterations = df_ask['book_id'].unique().shape[0]
    time_length = (df_ask['my_time'].max() - df_ask[
        'my_time'].min()).total_seconds()

    average_iter_time = time_length / number_of_iterations

    df.loc[0] = ['Number of ask pairs per time period',
                 50]
    df.loc[1] = ['Number of bid pairs per time period',
                 50]
    df.loc[2] = ['Sample time length in minutes',
                 time_length/60]
    df.loc[3] = ['Average time period in seconds',
                 average_iter_time]
    df.loc[4] = ['Number of time periods',
                 number_of_iterations]
    df.loc[5] = ['Total number of observational pairs',
                 number_of_iterations * 50 * 2]
    df.to_latex(buf=tables + 'sample_summary.tex', index=False)


def table_sample_summary_session_4() -> None:
    """
        Prepares the tables summarising sample
    """
    sql_ask = qry('basic_sorted4')[0]

    df_ask = sql_to_df(sql_ask)

    df_ask.groupby('book_id').count()

    df = pd.DataFrame(columns=['Definition', 'Value'])

    number_of_iterations = df_ask['book_id'].unique().shape[0]
    time_length = (df_ask['my_time'].max() - df_ask[
        'my_time'].min()).total_seconds()

    average_iter_time = time_length / number_of_iterations

    df.loc[0] = ['Number of ask pairs per time period',
                 50]
    df.loc[1] = ['Number of bid pairs per time period',
                 50]
    df.loc[2] = ['Sample time length in minutes',
                 time_length/60]
    df.loc[3] = ['Average time period in seconds',
                 average_iter_time]
    df.loc[4] = ['Number of time periods',
                 number_of_iterations]
    df.loc[5] = ['Total number of observational pairs',
                 number_of_iterations * 50 * 2]
    df.to_latex(buf=tables + 'sample_summary4.tex', index=False)



