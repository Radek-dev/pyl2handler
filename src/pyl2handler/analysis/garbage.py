
def taking_averages_for_prices_volumes() -> None:
    sql_ask, sql_bid = qry('basic_sorted')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    # should be like this
    # prices = np.arange(df_ask['price'].min(), df_ask['price'].max()+0.1,0.1, dtype=np.float64).round(1)

    prices = sorted(df_ask['price'].unique())
    df_v_a = pd.DataFrame(columns=prices,
                          index=pd.to_datetime(
                              sorted(df_ask['my_time'].unique())))

    for price in prices:
        df = df_ask[df_ask['price'] == price][['quantity', 'my_time']]
        df.rename(columns={'quantity': price}, inplace=True)
        df_v_a.loc[df['my_time'], price] = df[price].values
    del price

    df_v_a.index = df_v_a.index.values
    df_v_a.replace(np.nan, 0, inplace=True)
    df_v_a = df_v_a.cumsum(axis=1)
        df_v_a[::30][9177.7].plot(marker='.')
        plt.show()
    v_size = 7
    cols = ['Limit' + str(i) for i in range(v_size)]

    df_limits = pd.DataFrame(
        data=np.linspace(df_v_a.replace(0, np.nan).min(axis=1).values,
                         df_v_a[df_v_a.columns.max()].values, v_size).T,
        columns=cols,
        index=df_v_a.index.values)
    df_series_vol = pd.DataFrame(columns=cols, index=df_v_a.index)
    df_series_price = pd.DataFrame(columns=cols, index=df_v_a.index)

    time, row = next(df_v_a.iterrows())
    col = 'Limit0'

    i = 0
    for time, row in (df_v_a.iterrows()):
        for col in ['Limit' + str(i) for i in range(v_size-1)]:
            if col == 'Limit0':
                df_series_vol.loc[time, 'Limit0'] = \
                    row[row == df_limits.loc[time, 'Limit0']].array.median()
                df_series_price.loc[time, 'Limit0'] = \
                    np.median(row[row == df_limits.loc[time, 'Limit0']].index)
            elif col == cols[-1]:
                break
                df_series_vol.loc[time, 'Limit9'] = \
                    row[row == df_limits.loc[time, 'Limit9']].array
                df_series_price.loc[time, 'Limit9'] = \
                    np.median(row[row == df_limits.loc[time, 'Limit9']].index)
            else:
                med_qty = \
                    np.nanmedian(np.where((row.array >= df_limits.loc[time, col]) & (
                    row.array <= df_limits.loc[time, cols[cols.index(col) + 1]]), row.array,
                    np.nan))

                med_price = \
                    np.nanmedian(np.where((row.array >= df_limits.loc[time, col]) &
                    (row.array <= df_limits.loc[time, cols[cols.index(col) + 1]]),
                    row.index, np.nan))
                df_series_vol.loc[time, col] = med_qty
                df_series_price.loc[time, col] = med_price

    df_series_price.plot()

    df_limits
    row
    row.name

    df_series_price.plot()
    df_series_vol.plot()
    plt.show()

    np.where((df_v_a.iloc[0, :].values >= df_limits.iloc[0, 0]) &
             (df_v_a.iloc[0, :].values < df_limits.iloc[0, 1]), True, False)

    np.where((df_v_a.iloc[0, :].values >= df_limits.iloc[0, 8]) &
             (df_v_a.iloc[0, :].values <= df_limits.iloc[0, 9]), True, False)

    a = (df_v_a >= df_limits['Limit0'])
    a = df_v_a[(df_v_a >= df_limits['Limit0'])]
    np.where(df_v_a > df_limits['Limit0'], 0, 1)

    df_s.plot()
    df_limits.plot()
    plt.show()
    # df_v_a.to_excel('data_set.xlsx')


def find_var() -> None:
    if 'number_of_paths' not in locals():
        number_of_paths = 10

    sql_ask, sql_bid = qry('basic_sorted')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins =\
        pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                     number_of_paths), number_of_paths),
                     columns=['MinAsk' + str(i + 1) for i in
                              range(number_of_paths)])
    df_mins.index = pd.to_datetime(np.sort(df_ask['my_time'].unique()))

    df_maxs = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_maxs = pd.DataFrame(data=df_maxs.values.reshape(int(len(df_maxs) /
                           number_of_paths), number_of_paths),
                           columns=['MaxBid' + str(i + 1) for i in
                                    range(number_of_paths)])
    df_maxs.index = pd.to_datetime(np.sort(df_ask['my_time'].unique()))

    df = pd.concat([df_maxs['MaxBid1'], df_mins['MinAsk1']], axis=1)

    from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
    from statsmodels.tsa.api import VAR

    df.describe()

    df.plot()
    plt.show()

    plot_acf(df['MaxBid1'].diff().dropna())

    dfl = df.diff().dropna()
    plot_acf(dfl['MaxBid1'])
    plot_pacf(dfl['MaxBid1'])
    plot_acf(dfl['MinAsk1'])
    plot_pacf(dfl['MinAsk1'])
    plt.show()

    model = VAR(endog=dfl)
    res = model.select_order(15)
    print(res.summary())

    model_fit1 = model.fit(maxlags=1)
    model_fit2 = model.fit(maxlags=2)
    model_fit3 = model.fit(maxlags=3)

    model_fit1.summary()
    model_fit2.summary()
    model_fit3.summary()

    df['MaxBid1_avg_rol'], df['MinAsk1_avg_rol'] =\
        df['MinAsk1'].rolling(10).mean(), df['MinAsk1'].rolling(10).mean()

    df10 = df[::10]
    df10.plot()
    plt.show()

    a = df10.diff().dropna()

    model_10 = VAR(endog=df10)
    model_10.fit(1).summary()

    from statsmodels.tsa.vector_ar.vecm import VECM
    model = VECM(endog=df, k_ar_diff=1, coint_rank=2, deterministic='co')
    res = model.fit()
    res.summary()
    X_pred = res.predict(steps=10)

def dual_regression_gls() -> None:
    sql_ask = qry('one_point')[0]

    df_ask = sql_to_df(sql_ask)

    df_ask.sort_values('price', inplace=True)
    df_ask['cum_quantity_ask'] = df_ask['quantity'].cumsum()
    df_ask['const_ask'] = 1.0
    df_ask['price_log'] = np.log(df_ask['price'])

    fit_ask = sm.GLS(df_ask['price_log'], df_ask[
        ['cum_quantity_ask', 'const_ask']]).fit()

    fit_ask.summary()

    fit_ask = sm.GLSAR(df_ask['price_log'], df_ask[
        ['cum_quantity_ask', 'const_ask']]).fit()

    fit_ask.summary()

    fit_ask = sm.recursiveLS(df_ask['price_log'], df_ask[
        ['cum_quantity_ask', 'const_ask']]).fit()

    fit_ask.summary()