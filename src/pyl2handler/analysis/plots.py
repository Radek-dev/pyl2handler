import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdate
import pandas as pd
import seaborn as sns
from pyl2handler.io.pg import sql_to_df
from pyl2handler.helpers import store_queries as qry
import numpy as np

mpl.use('Qt5Agg')
sns.set()
figures = '/home/rad/projects/l2handler/Thesis/Figures/'
save_file = 0


def plot_some_quantities_at_one_point() -> None:
    """
        Creates a some_quantities_at_one_point figure for the report
    """

    sql_ask, sql_bid = qry('one_point')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask.rename(columns={'price': '1 BTC per USD',
                           'quantity': 'AskQuantity'}, inplace=True)
    df_bid.rename(columns={'price': '1 BTC per USD',
                           'quantity': 'BidQuantity'}, inplace=True)

    df_ask.set_index('1 BTC per USD', inplace=True)
    df_bid.set_index('1 BTC per USD', inplace=True)

    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    df_ask.plot(ax=ax[1], subplots=True,
                marker='.', linewidth=0.0001)
    df_bid.plot(ax=ax[0], subplots=True,
                marker='.', linewidth=0.0001, color='orange')
    ax[0].set_ylabel('Quantity - Percentage of 1 BTC')
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'some_quantities_at_one_point.png', dpi=300)
    else:
        plt.show()


def plot_quantities_based_on_zero() -> None:
    """
        Creates a quantities_based_on_zero figure for the report
        There are 4 of them here:
    """
    sql_ask, sql_bid = qry('basic_sorted')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_ask_min = df_ask.groupby(
        by=['book_id'], as_index=False).min()[['book_id', 'price']]
    df_bid_max = df_bid.groupby(
        by=['book_id'], as_index=False).max()[['book_id', 'price']]

    df_ask = pd.merge(df_ask, df_ask_min, on=['book_id'], how='left',
                      suffixes=('', '_min'))
    df_bid = pd.merge(df_bid, df_bid_max, on=['book_id'], how='left',
                      suffixes=('', '_max'))

    df_ask['distance_to_min'] = df_ask['price'] - df_ask['price_min']
    df_bid['distance_to_max'] = df_bid['price'] - df_bid['price_max']

    df_ask_one_point = df_ask[df_ask['book_id'] == 801550895]

    sns.set(font_scale=1.325)

    plt.figure(figsize=(15, 8))
    plt.scatter(df_ask_one_point['distance_to_min'],
                df_ask_one_point['quantity'],
                label='Normalised ask Prices at One Time Period')
    plt.xlabel('Normalised Ask Prices at One Time Period')
    plt.ylabel('Ask Quantity')
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'quantities_based_on_zero_ask_one_point.png',
                    dpi=300)
    else:
        plt.show()

    plt.figure(figsize=(15, 8))
    plt.scatter(df_ask['distance_to_min'], df_ask['quantity'], cmap='jet',
                c=mpl.dates.date2num(df_ask['my_time']))
    plt.colorbar(ticks=mpl.dates.SecondLocator(interval=60),
                 format=mpl.dates.DateFormatter('%H:%M'))
    plt.ylabel('Ask Quantity')
    plt.xlabel('Normalised Ask Prices over Time')
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'quantities_based_on_zero_ask.png', dpi=300)
    else:
        plt.show()

    plt.figure(figsize=(15, 8))
    plt.scatter(df_ask['price'], df_ask['quantity'], cmap='jet',
                c=mpl.dates.date2num(df_ask['my_time']))
    plt.colorbar(ticks=mpl.dates.SecondLocator(interval=60),
                 format=mpl.dates.DateFormatter('%H:%M'))
    plt.ylabel('Ask Quantity')
    plt.xlabel('Ask Prices over Time')
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'quantities_ask.png', dpi=300)
    else:
        plt.show()

    plt.figure(figsize=(15, 8))
    plt.scatter(df_bid['distance_to_max'], df_bid['quantity'], cmap='jet',
                c=mpl.dates.date2num(df_bid['my_time']))
    plt.colorbar(ticks=mpl.dates.SecondLocator(interval=60),
                 format=mpl.dates.DateFormatter('%H:%M'))
    plt.ylabel('Bid Quantity')
    plt.xlabel('Bid Prices over Time')
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'quantities_based_on_zero_bid.png', dpi=300)
    else:
        plt.show()

    plt.figure(figsize=(15, 8))
    plt.scatter(df_bid['price'], df_bid['quantity'], cmap='jet',
                c=mpl.dates.date2num(df_bid['my_time']))
    plt.colorbar(ticks=mpl.dates.SecondLocator(interval=60),
                 format=mpl.dates.DateFormatter('%H:%M'))
    plt.ylabel('Bid Quantity')
    plt.xlabel('Normalised Bid Prices over Time')
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'quantities_bid.png', dpi=300)
    else:
        plt.show()


def plot_usds_over_time() -> None:
    sql_ask = qry('basic_sorted')[0]

    df_ask = sql_to_df(sql_ask)

    plt.figure(figsize=(15, 8))
    plt.scatter(df_ask['price'], df_ask['total_usd'], cmap='jet',
                c=mpl.dates.date2num(df_ask['my_time']))
    plt.colorbar(ticks=mpl.dates.SecondLocator(interval=60),
                 format=mpl.dates.DateFormatter('%H:%M'))
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'quantities_ask.png', dpi=300)
    else:
        plt.show()


def plot_some_price_over_time_with_colorbar() -> None:
    """
        Test plotting
    """

    sql_ask, sql_bid = qry('quantities_over_time')

    df_ask = sql_to_df(sql_ask)

    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)

    plt.figure(figsize=(15, 8))
    plt.scatter(df_ask_min['my_time'], df_ask_min['price'], cmap='jet',
                c=mpl.dates.date2num(df_ask_min['my_time']))
    plt.colorbar(ticks=mpl.dates.SecondLocator(interval=60),
                 format=mpl.dates.DateFormatter('%H:%M'))
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'min_ask_max_bid_over_time_test.png', dpi=300)
    else:
        plt.show()


def plot_min_ask_max_bid_over_time() -> None:
    """
        Creates a min_ask_max_bid_over_time figure for the report
    """
    sql_ask, sql_bid = qry('basic')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)
    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]
    df_bid_max = df_bid.groupby(by=['book_id']).max()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)
    df_bid_max.sort_values(by=['my_time'], inplace=True)

    fig, ax = plt.subplots()
    plt.plot(df_ask_min['my_time'], df_ask_min['price'],
             marker='.', label='Ask Price')
    plt.plot(df_bid_max['my_time'], df_bid_max['price'],
             marker='.', label='Bid Price')
    plt.legend()
    ax.set_ylabel('1 BTC in USD')
    ax.set_xlabel('Time on 28th June 2020')
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
    fig.autofmt_xdate()
    plt.tight_layout()
    if save_file:
        fig.savefig(figures + 'min_ask_max_bid_over_time.png', dpi=300)
    else:
        plt.show()


def plot_5_best_bid_ask_over_time(number_of_paths: int = 5) -> None:
    """
        Creates a 5_best_bid_ask_over_time figure for the report
    """
    if 'number_of_paths' not in locals():
        number_of_paths = 5

    sql_ask, sql_bid = qry('basic_sorted')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins = pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                                                           number_of_paths),
                                                       number_of_paths),
                           columns=['MinAsk' + str(i + 1) for i in
                                    range(number_of_paths)])

    df_maxs = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_maxs = pd.DataFrame(data=df_maxs.values.reshape(int(len(df_maxs) /
                                                           number_of_paths),
                                                       number_of_paths),
                           columns=['MaxBid' + str(i + 1) for i in
                                    range(number_of_paths)])

    sns.set(font_scale=2)
    df = pd.concat([df_maxs, df_mins], axis=1, sort=False)

    df.index = pd.to_datetime(
        df_ask.groupby(['book_id']).min()['my_time'].values)

    ax = df.plot(marker='.', figsize=(15, 10))
    ax.set_ylabel('1 BTC in USD')
    ax.set_xlabel('Time on 28th June 2020')
    plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
    plt.tight_layout()
    plt.show()


def plot_3_best_ask_quantities_over_time(number_of_paths: int = 3) -> None:
    """
        Creates a 3_best_ask_quantities_over_time figure for the report
    """
    if 'number_of_paths' not in locals():
        number_of_paths = 3

    sql_ask, _ = qry('basic_sorted')
    df_ask = sql_to_df(sql_ask)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_id_lines = pd.DataFrame(
        data=zip(df_mins.values, df_mins.index.get_level_values(0)),
        columns=['price', 'book_id'])

    df_qties = pd.merge(df_id_lines, df_ask[['book_id', 'price', 'quantity']],
                        on=['book_id', 'price'], how='left')

    df = pd.DataFrame(data=df_qties['quantity'].values.reshape(
        int(len(df_qties) / number_of_paths), number_of_paths),
        columns=['MinQty' + str(i + 1) for i in
                 range(number_of_paths)],
        index=df_ask.groupby(['book_id']).min()['my_time'].values)

    sns.set(font_scale=2)
    plt.figure(figsize=(15, 10))
    plt.scatter(df.index, df['MinQty3'], color='orange',
                label='Quantity With the 3rd Smallest Ask Price Quote')

    plt.scatter(df.index, df['MinQty2'], color='blue',
                label='Quantity With the 2nd Smallest Ask Price Quote')

    plt.scatter(df.index, df['MinQty1'], color='green',
                label='Quantity With the Smallest Ask Price Quote')
    plt.xlabel('Time on 28th June 2020')
    plt.ylabel('Quantity - Percentage of 1 BTC')
    plt.legend()
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + '3_best_ask_quantities_over_time.png', dpi=300)
    else:
        plt.show()


def plot_best_bid_ask_spread_over_time() -> None:
    """
        Creates a spread_over_time figure for the report
    """
    sql_ask, sql_bid = qry('basic')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)
    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]
    df_bid_max = df_bid.groupby(by=['book_id']).max()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)
    df_bid_max.sort_values(by=['my_time'], inplace=True)

    df_spread = pd.DataFrame(
        data=(df_ask_min['price'] - df_bid_max['price']).values,
        index=df_ask_min['my_time'], columns=['spread'])

    fig, ax = plt.subplots()
    ax = df_spread.plot(marker='o')
    plt.legend()
    fig.autofmt_xdate()
    ax.set_ylabel('Spread for 1 BTC in USD')
    ax.set_xlabel('Time on 28th June 2020')
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
    plt.tight_layout()
    if save_file:
        plt.savefig(figures + 'best_bid_ask_spread_over_time.png', dpi=300)
    else:
        plt.show()


def plot_3d_price_qty_over_time(number_of_paths: int = 1) -> None:
    if 'number_of_paths' not in locals():
        number_of_paths = 1

    sql_ask, sql_bid = qry('basic_sorted')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_id_mins = pd.DataFrame(
        data=zip(df_mins.values, df_mins.index.get_level_values(0)),
        columns=['price', 'book_id'])
    df_ask_mins = pd.merge(df_id_mins,
                           df_ask[['book_id', 'price', 'quantity', 'my_time']],
                           on=['book_id', 'price'], how='left')

    df_max = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_id_max = pd.DataFrame(
        data=zip(df_max.values, df_max.index.get_level_values(0)),
        columns=['price', 'book_id'])
    df_bid_max = pd.merge(df_id_max,
                          df_bid[['book_id', 'price', 'quantity', 'my_time']],
                          on=['book_id', 'price'], how='left')

    ax = plt.figure().gca(projection='3d')
    ax.scatter(df_ask_mins.index, df_ask_mins['price'],
               df_ask_mins['quantity'], label='Min Ask Price with Quantity')
    ax.scatter(df_bid_max.index, df_bid_max['price'],
               df_bid_max['quantity'], label='Max Bid Price with Quantity',
               color='orange')
    ax.legend()
    ax.set_xlabel('Time index')
    ax.set_ylabel('Price')
    ax.set_zlabel('Quantity')
    plt.tight_layout()
    plt.show()


def plot_3d_price_qty_over_time_colorbar(number_of_paths: int = 1) -> None:
    if 'number_of_paths' not in locals():
        number_of_paths = 1

    sql_ask, sql_bid = qry('basic_sorted')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_id_mins = pd.DataFrame(
        data=zip(df_mins.values, df_mins.index.get_level_values(0)),
        columns=['price', 'book_id'])

    df_ask_min = \
        pd.merge(df_id_mins,
                 df_ask[['book_id', 'price', 'quantity', 'my_time']],
                 on=['book_id', 'price'], how='left')

    df_max = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_id_max = pd.DataFrame(
        data=zip(df_max.values, df_max.index.get_level_values(0)),
        columns=['price', 'book_id'])
    df_bid_max = pd.merge(df_id_max,
                          df_bid[['book_id', 'price', 'quantity', 'my_time']],
                          on=['book_id', 'price'], how='left')

    sns.set(font_scale=1.3)
    fig = plt.figure(figsize=(15, 10))
    ax = fig.gca(projection='3d')
    p = ax.scatter(df_ask_min.index, df_ask_min['price'],
                   df_ask_min['quantity'], label='Min Ask Price with Quantity',
                   c=mpl.dates.date2num(df_ask_min['my_time']), cmap='winter')
    cbar = fig.colorbar(p, ticks=mpl.dates.SecondLocator(interval=60),
                        format=mpl.dates.DateFormatter('%H:%M'))
    cbar.set_label('Time')
    p = ax.scatter(df_bid_max.index, df_bid_max['price'],
                   df_bid_max['quantity'], label='Max Bid Price with Quantity',
                   c=mpl.dates.date2num(df_ask_min['my_time']), cmap='cool')
    cbar = fig.colorbar(p, ticks=mpl.dates.SecondLocator(interval=60),
                        format=mpl.dates.DateFormatter('%H:%M'))
    cbar.set_label('Time')
    ax.legend()
    ax.set_xlabel('Time index')
    ax.set_ylabel('Price')
    ax.set_zlabel('Quantity')
    plt.tight_layout()
    plt.show()


def plot_min_ask_max_bid_over_time_2() -> None:
    """
        Creates a min_ask_max_bid_over_time_2 figure for the report
    """
    sql_ask, sql_bid = qry('basic4')

    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)
    df_ask_min = df_ask.groupby(by=['book_id']).min()[['price', 'my_time']]
    df_bid_max = df_bid.groupby(by=['book_id']).max()[['price', 'my_time']]

    df_ask_min.sort_values(by=['my_time'], inplace=True)
    df_bid_max.sort_values(by=['my_time'], inplace=True)

    fig, ax = plt.subplots()
    plt.plot(df_ask_min['my_time'], df_ask_min['price'],
             marker='.', label='Ask Price')
    plt.plot(df_bid_max['my_time'], df_bid_max['price'],
             marker='.', label='Bid Price')
    plt.legend()
    ax.set_ylabel('1 BTC in USD')
    ax.set_xlabel('Time on 12th-13th August 2020')
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
    fig.autofmt_xdate()
    plt.tight_layout()
    if save_file:
        fig.savefig(figures + 'min_ask_max_bid_over_time_2.png', dpi=300)
    else:
        plt.show()


def plot_5_best_bid_ask_over_time_2(number_of_paths: int = 5) -> None:
    """
        Creates a 5_best_bid_ask_over_time figure for the report
    """
    if 'number_of_paths' not in locals():
        number_of_paths = 5

    sql_ask, sql_bid = qry('basic_sorted4')
    df_ask, df_bid = sql_to_df(sql_ask), sql_to_df(sql_bid)

    df_mins = df_ask.groupby('book_id')['price'].nsmallest(number_of_paths)
    df_mins =\
        pd.DataFrame(data=df_mins.values.reshape(int(len(df_mins) /
                     number_of_paths), number_of_paths),
                     columns=['MinAsk' + str(i + 1) for i in
                                    range(number_of_paths)])

    df_maxs = df_bid.groupby('book_id')['price'].nlargest(number_of_paths)
    df_maxs =\
        pd.DataFrame(data=df_maxs.values.reshape(int(len(df_maxs) /
                     number_of_paths), number_of_paths),
                     columns=['MaxBid' + str(i + 1) for i in
                                    range(number_of_paths)])

    df_maxs.index = pd.to_datetime(
        df_ask.groupby(['book_id']).min()['my_time'].values)
    df_mins.index = pd.to_datetime(
        df_ask.groupby(['book_id']).min()['my_time'].values)

    df_maxs = df_maxs[::30]
    df_mins = df_mins[::30]

    sns.set(font_scale=2)
    df = pd.concat([df_maxs, df_mins], axis=1, sort=False)

    ax = df.plot(marker='.', figsize=(15, 10))
    ax.set_ylabel('1 BTC in USD')
    ax.set_xlabel('Time on 12th-13th August 2020')
    plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%H:%M'))
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    plot_price_volume_layers_over_time()
