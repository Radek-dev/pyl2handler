import psycopg2
import pytest
from pyl2handler.io.pg import get_version


@pytest.mark.skip(msg="Don't work in CI")
def test_get_version():
    try:
        get_version()
    except psycopg2.DatabaseError:
        get_version(('io', 'credentials_test.ini'))
    with pytest.raises(psycopg2.OperationalError):
        get_version(('io', 'credentials_test.ini'))
