from asyncio import get_event_loop

import pytest
from pyl2handler.helpers import get_config
from pyl2handler.io.cexiorest import CEXRestClient


@pytest.mark.skip(msg="Don't work in CI")
def test_cexiorest():
    loop = get_event_loop()
    try:
        conf = get_config(section='cexio_home')
        client = CEXRestClient(conf)
        loop.run_until_complete(client.get("currency_limits"))
        loop.run_until_complete(
            client.post("order_book/BTC/USD", {'depth': ''}))
        loop.run_until_complete(client.get("ohlcv/hd/20160228/BTC/USD"))
        loop.run_until_complete(client.post("balance/", auth=True))
        loop.run_until_complete(client.post("open_orders/BTC/USD/", auth=True))
    finally:
        loop.close()


@pytest.mark.xfail(raises=Exception)
def test_cexiorest_error_id():
    loop = get_event_loop()
    try:
        conf = get_config(section='cexio_test_id',
                          filename=('io', 'credentials_test.ini'))
        client = CEXRestClient(conf)
        loop.run_until_complete(client.post("balance/", auth=True))
    finally:
        loop.close()


@pytest.mark.xfail(raises=Exception)
def test_cexiorest_error_rest():
    loop = get_event_loop()
    try:
        conf = get_config(section='cexio_test_rest',
                          filename=('io', 'credentials_test.ini'))
        client = CEXRestClient(conf)
        loop.run_until_complete(client.post("balance/", auth=True))
    finally:
        loop.close()
