import asyncio as aio

import logging as lg
import logging.config

from pyl2handler.helpers import get_path

logging.config.fileConfig(get_path(('logging_config.ini',), True))
file_logger = lg.getLogger('fileLogger')
console_logger = lg.getLogger('simpleExample')


async def write_bid():
    await aio.sleep(3)
    file_logger.debug('writing bid')
    console_logger.debug('writing bid')


async def write_ask():
    await aio.sleep(10)
    file_logger.debug('writing ask')
    console_logger.debug('writing ask')


async def make_request():
    for i in range(5):
        await aio.sleep(1)
        aio.ensure_future(write_ask())
        aio.ensure_future(write_bid())
        file_logger.debug('making a request')
        console_logger.debug('making a request')
    await aio.sleep(12)


def main():
    loop = aio.get_event_loop()
    try:
        loop.run_until_complete(make_request())
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()


if __name__ == '__main__':
    main()
