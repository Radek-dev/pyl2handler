# -*- coding: utf-8 -*-

import pytest

from pyl2handler.skeleton import fib

__author__ = "Radek.Chramosil"
__copyright__ = "Radek.Chramosil"
__license__ = "simple-bsd"


def test_fib():
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
