pyl2handler.analysis package
============================

Submodules
----------

pyl2handler.analysis.plots module
---------------------------------

.. automodule:: pyl2handler.analysis.plots
   :members:
   :undoc-members:
   :show-inheritance:

pyl2handler.analysis.stats\_modules module
-----------------------------------------

.. automodule:: pyl2handler.analysis.stats_modules
   :members:
   :undoc-members:
   :show-inheritance:

pyl2handler.analysis.stats\_tests module
----------------------------------------

.. automodule:: pyl2handler.analysis.stats_tests
   :members:
   :undoc-members:
   :show-inheritance:

pyl2handler.analysis.tables module
----------------------------------

.. automodule:: pyl2handler.analysis.tables
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: pyl2handler.analysis
   :members:
   :undoc-members:
   :show-inheritance:
