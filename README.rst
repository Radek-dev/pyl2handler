===========
pyl2handler
===========
Finacial Level 2 Data Handler

Description
===========
This project aims to handle Level 2 Data, specifically collection and probabilistic modelling.


Installation pyl2handler
========================
The package uses `psycopg2` to connect to AWS Postgres instance.
See the detailed installation instruction for `psycopg2 <https://www.psycopg.org/docs/install.html>`_
Once you have all requirements installed locally (install postgres server locally), run::

    pip install psycopg2

To install ``pyl2handler`` use::

    pip install -e .

Developer Installation for pyl2handler
======================================
All developer requirements are stored in ``dev-requirements.txt``. Run the below to get the dev environment set up::

    pip install -r dev-requirements.txt

This should be done only after all other requirements are installed locally.

Pre-commit Note
===============
It is good idea to use `pre-commit <https://pre-commit.com/>`_.

A `.pre-commit-config.yaml` file was generated inside your project but in order to make sure the hooks will run, please don't forget to install the `pre-commit` package::

  cd test
  # it is a good idea to create and activate a virtualenv here
  pip install pre-commit
  pre-commit install
  # another good idea is update the hooks to the latest version
  # pre-commit autoupdate


To run the pre-commit::

    pre-commit run --all-files

If you don't have the pre-commit ``yaml`` file, run::

    git add .pre-commit-config.yaml


Installing Python 3.8 on Ubuntu
===============================
The project supports Python 3.7 and 3.8 and uses ``tox`` to test the builds.
See `this page <https://sourcedigit.com/24412-how-to-install-python-3-8-in-ubuntu/>`_
to install python3.8 on Ubuntu::

    sudo apt-get update
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt-get update
    sudo apt install python3.8

Documentation PDF
=================
To build the documentation::

    sphinx-build -M latexpdf docs build/pdf
    python3.7 setup.py docs

Note
====
This project has been set up using PyScaffold 3.2.3. For details and usage
information on PyScaffold see https://pyscaffold.org/.
